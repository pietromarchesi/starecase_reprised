import os
import pandas as pd
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import pickle
import matplotlib.pyplot as plt
import seaborn as sn
from constants import *
from starecase_plotting_style import *
import subprocess
import time
from dec_clust.dec_clust_utils import timer
import distutils.util

try:
    __IPYTHON__

except NameError:

    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--method', type=str)
    parser.add_argument('--algorithm', type=str)
    parser.add_argument('--score', type=str)
    parser.add_argument('--n_sample', type=int)
    parser.add_argument('--n_repeats', type=int)
    parser.add_argument('--debias', type=distutils.util.strtobool, default=0)


    ARGS = parser.parse_args()

    method = ARGS.method
    algorithm = ARGS.algorithm
    score = ARGS.score
    n_sample = ARGS.n_sample
    n_repeats = ARGS.n_repeats
    debias = ARGS.debias

else:
    n_sample  = 30
    n_repeats = 300
    method    = 'dec'    # 'clust' o 'dec'
    algorithm = 'naive_bayes'
    score     = 'auc'
    debias    = True

stim = [1, 3]

if method == 'dec':
    data_dir = os.path.join(results_root, 'decoding')
elif method == 'clust':
    data_dir = os.path.join(results_root, 'clustering')

if debias:
    debias_string = 'debiased_'
else:
    debias_string = ''

if isinstance(stim, int):
    stim = [stim]


if not os.path.isdir(os.path.join(data_dir, 'lme')):
    os.makedirs(os.path.join(data_dir, 'lme'))

for st in stim:

    file = '{}_response_on_raw_traces_{}_n{}_s{}_r{}.pkl'.format(method,
            algorithm, n_sample, st, n_repeats)

    C = pickle.load(open(os.path.join(data_dir, file), 'rb'))['results']
    try:
        C.__delitem__('pars')
    except:
        pass
    all_scores = [sc for sc in C.keys() if sc[-8:] != 'shuffled']

    n_time = C[all_scores[0]][mouse_ids[0]]['AL'].shape[1]
    n_samp = C[all_scores[0]][mouse_ids[0]]['AL'].shape[0]

    columns = ['frame', 'area', 'mouse_id'] + all_scores

    #df = pd.DataFrame(columns=columns)
    dfx_list = []
    for frame in range(n_time):
        for mouse_id, area in datasets:
            x = {'area' : area,
                 'mouse_id' : mouse_id,
                 'frame' : frame}
            for sc in all_scores:
                x1 = C[sc][mouse_id][area][:, frame]
                if debias:
                    x2 = C['{}_shuffled'.format(sc)][mouse_id][area][:, frame]
                    # or: x2 = C['{}_shuffled'.format(sc)][mouse_id][area][:, frame].mean()
                    x[sc] = x1 - x2
                else:
                    x[sc] =  x1
            dfx = pd.DataFrame(x)
            dfx = dfx[columns]
            dfx_list.append(dfx)

    df = pd.concat(dfx_list)

    input_file_name = 'lme_{}_data_{}{}_n{}_s{}_r{}.csv'.format(method, debias_string,
                      algorithm, n_sample, st, n_repeats)
    input_file_path = os.path.join(data_dir, 'lme', input_file_name)

    output_file_name = 'lme_{}_results_{}{}_{}_n{}_s{}_r{}.csv'.format(method, debias_string,
                       algorithm, score, n_sample, st, n_repeats)
    output_file_path = os.path.join(data_dir, 'lme',  output_file_name)

    print('Saving intermediate output to:\n> {}'.format(input_file_path))

    df.to_csv(input_file_path)

    print('Running LME for score {}'.format(score))
    t1 = time.time()
    subprocess.call(["Rscript",
                     "--vanilla",
                     "./mixed_effects.R",
                     input_file_path, output_file_path, method, algorithm,
                     str(n_sample),  str(st), str(n_repeats), score])
    t2 = time.time()
    print(' -> Completed in {}'.format(timer(t1, t2)))
    print('Saving LME output to:\n> {}'.format(output_file_path))
