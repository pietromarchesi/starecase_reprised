#!/usr/bin/env bash


sbatch ./run_clustering.sh 300 1 'KMeans' 1
sbatch ./run_clustering.sh 300 3 'KMeans' 1
#sbatch ./run_clustering.sh 1000 1 'MiniBatchKMeans' 1
#sbatch ./run_clustering.sh 1000 3 'MiniBatchKMeans' 1

sbatch ./run_decoding.sh 300 1 'naive_bayes' 1
sbatch ./run_decoding.sh 300 3 'naive_bayes' 1

sbatch ./run_decoding.sh 300 1 'logistic_regression' 1
sbatch ./run_decoding.sh 300 3 'logistic_regression' 1

sbatch ./run_decoding.sh 300 1 'random_forest' 1
sbatch ./run_decoding.sh 300 3 'random_forest' 1



#sbatch ./run_decoding_rt.sh 300 1 'naive_bayes' 1
#sbatch ./run_decoding_rt.sh 300 3 'naive_bayes' 1
#
#sbatch ./run_decoding_rt.sh 300 1 'logistic_regression' 1
#sbatch ./run_decoding_rt.sh 300 3 'logistic_regression' 1
