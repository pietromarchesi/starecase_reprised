import subprocess

methods = ['clust', 'dec']

# scores  = {'dec'   : ['precisionP', 'recallP', 'fbetaP',
#                       'precisionN', 'recallN', 'fbetaN',
#                       'accuracy', 'auc'],
#            'clust' : ['rand', 'mi', 'ami', 'nmi',
#                       'h', 'c', 'v', 'fm']}

scores  = {'dec'   : ['fbetaN', 'auc'],
           'clust' : ['ami', 'v']}

# algorithms = {'dec'   : ['naive_bayes', 'random_forest'],
#               'clust' : ['MiniBatchKMeans', 'KMeans']}

# algorithms = {'dec' : ['random_forest', 'naive_bayes', 'logistic_regression'],
#               'clust' : ['KMeans']}

algorithms = {'dec' : ['naive_bayes', 'logistic_regression', 'random_forest'],
              'clust' : ['KMeans']}

#sample_sizes = [10, 20, 30, 40, 50]
sample_sizes = [30]
n_repeats = 300
decode_var = 'response'
err_style = 'ci_band'
ci_trace  = 'stderr'
ci_diff   = 'stderr'
plot_format = 'svg'
dpi         = 600

for n_sample in sample_sizes:
    for method in methods:
        for algorithm in algorithms[method]:
            for score in scores[method]:
                for debias, plot_shuffled in [[1, 0]]:#[[0, 1], [1, 0]]:
                    print('\nMethod: {} \nAlgorithm: {} \nScore: {} '\
                          '\n# sample: {} \n# repeats: {}'.format(method, algorithm, score,
                                                                  n_sample, n_repeats))

                    subprocess.call(['python',
                                     './plot_dec_clust_over_time.py',
                                     '--n_sample', str(n_sample),
                                     '--n_repeats', str(n_repeats),
                                     '--method', method,
                                     '--algorithm', algorithm,
                                     '--score', score,
                                     '--decode_var', decode_var,
                                     '--average_repeats', str(1),
                                     '--debias', str(debias),
                                     '--plot_shuffled', str(plot_shuffled),
                                     '--err_style', err_style,
                                     '--ci_trace', str(ci_trace),
                                     '--ci_diff', str(ci_diff),
                                     '--save_plots', str(1),
                                     '--plot_format', plot_format,
                                     '--dpi', str(dpi)])




