#!/bin/bash

#SBATCH --job-name=clust_raw
#SBATCH --output=clust_starecase_%A_%a.out
#SBATCH --error=clust_starecase_%A_%a.err
#SBATCH --array=10,20,30,40,50
#SBATCH --partition=cpu
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=5000
#SBATCH --exclude=csn-cpu[1-5]

source ~/venvs/basic36/bin/activate
python ./clustering_on_raw_traces.py -n $SLURM_ARRAY_TASK_ID -r $1 -s $2 -a $3 -d $4
