import subprocess

methods = ['dec', 'clust']

# scores  = {'dec'   : ['precisionP', 'recallP', 'fbetaP',
#                       'precisionN', 'recallN', 'fbetaN',
#                       'accuracy', 'auc'],
#            'clust' : ['rand', 'mi', 'ami', 'nmi',
#                       'h', 'c', 'v', 'fm']}

scores  = {'dec'   : ['fbetaN', 'auc'],
           'clust' : ['ami', 'nmi', 'v']}

algorithms = {'dec'   : ['naive_bayes', 'random_forest', 'logistic_regression'],
              'clust' : ['KMeans']}


sample_sizes = [10, 20, 30, 40, 50]
#sample_sizes = [30]
n_repeats = 300
#debias = 1

for method in methods:
    for algorithm in algorithms[method]:
        for score in scores[method]:
            for n_sample in sample_sizes:
                for debias in [0, 1]:
                    print('\nMethod: {} \nAlgorithm: {} \nScore: {} '\
                          '\n# sample: {} \n# repeats: {}'.format(method, algorithm, score,
                                                                  n_sample, n_repeats))

                    try:
                        subprocess.call(['python',
                                         './run_lme.py',
                                         '--method', method,
                                         '--algorithm', algorithm,
                                         '--score', score,
                                         '--n_sample', str(n_sample),
                                         '--n_repeats', str(n_repeats),
                                         '--debias', str(debias)])

                    except FileNotFoundError:
                        print('>>> No file exists for the current settings <<<')