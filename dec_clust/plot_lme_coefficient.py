import os
import pandas as pd
import scipy
from statsmodels.stats.multitest import multipletests
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from starecase_plotting_style import *
from constants import *
import distutils.util
from scipy.stats import norm


try:
    __IPYTHON__
except NameError:

    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--n_sample', type=int, default=30)
    parser.add_argument('--n_repeats', type=int, default=3000)

    parser.add_argument('--method', type=str, choices=['clust', 'dec'])
    parser.add_argument('--algorithm', type=str, choices=['random_forest',
                                                          'naive_bayes',
                                                          'logistic_regression',
                                                          'KMeans'])
    parser.add_argument('--score', type=str, choices=['precisionP', 'recallP', 'fbetaP',
                                                       'precisionN', 'recallN', 'fbetaN',
                                                       'accuracy', 'auc',
                                                       'rand', 'mi', 'ami', 'nmi',
                                                       'h', 'c', 'v', 'fm'])
    parser.add_argument('--debias', type=distutils.util.strtobool, default=0)
    parser.add_argument('--multitest', type=str, default='Bonferroni')
    parser.add_argument('--plot_format', type=str, default='png')
    parser.add_argument('--big_font', type=distutils.util.strtobool, default=0)
    parser.add_argument('--dpi', type=int, default=500)

    ARGS = parser.parse_args()

    n_sample        = ARGS.n_sample
    n_repeats       = ARGS.n_repeats
    method          = ARGS.method
    algorithm       = ARGS.algorithm
    score           = ARGS.score
    debias          = ARGS.debias
    multitest       = ARGS.multitest
    plot_format     = ARGS.plot_format
    big_font        = ARGS.big_font
    dpi             = ARGS.dpi

else:

    n_sample            = 30
    n_repeats           = 300
    method              = 'dec'    # 'clust' o 'dec'
    algorithm           = 'random_forest' # MiniBatchKMeans, #KMeans, #random_forest, #naive_bayes
    score               = 'auc'
    debias              = True
    multitest           = 'b' # fdr_bh
    plot_format         = 'svg'
    big_font            = True
    dpi                 = 500

if big_font:
    import matplotlib
    matplotlib.rcParams.update({'font.size': 16})


# FIXED PARAMETERS
stim_types      = [1, 3]
smooth          = False
sigma           = 1
time            = np.arange(-12, 76)
save_plots      = True
p_val_threshold = 0.01

plots_dir       = os.path.join(results_root, 'plots', 'lme')

if not os.path.isdir(plots_dir):
    os.makedirs(plots_dir)

if method == 'dec':
    data_dir = os.path.join(results_root, 'decoding', 'lme')
elif method == 'clust':
    data_dir = os.path.join(results_root, 'clustering', 'lme')

if debias:
    debias_string = 'debiased_'
else:
    debias_string = ''


L = {s : None for s in stim_types}
for stim in stim_types:

    lme_file = 'lme_{}_results_{}{}_{}_n{}_s{}_r{}.csv'.format(method,
                debias_string, algorithm, score, n_sample, stim, n_repeats)

    L_stim  = pd.read_csv(os.path.join(data_dir, lme_file))
    L[stim] = L_stim
#scores = [score for score in C[stim_types[0]].keys()]



# --- ADJUST P-VALUES ---------------------------------------------------------


for stim in stim_types:
    reject, p_val_adj, als, alb = multipletests(L[stim]['p_val'], method=multitest)
    L[stim]['p_val_adj'] = p_val_adj

z_statistic = []
p_val_diff = []
for j in range(L[stim]['coeff'].shape[0]):
    x1 = L[1]['coeff'].iloc[j]
    x2 = L[3]['coeff'].iloc[j]
    se1 = L[1]['stder'].iloc[j]
    se2 = L[3]['stder'].iloc[j]
    Z = (x1 - x2) / np.sqrt((se1**2+se2**2))
    z_statistic.append(np.abs(Z))
    p = scipy.stats.norm.sf(abs(Z))*2
    p_val_diff.append(p)
reject, p_val_diff_adj, als, alb = multipletests(p_val_diff, method=multitest)




# --- PLOT LME COEFFICIENT ----------------------------------------------------




f, ax = plt.subplots(1, 1, figsize=[6, 4])
for stim in stim_types:
    y = -L[stim]['coeff']
    err = L[stim]['stder']
    ax.plot(time, y, c=stimtype_colormap[stim])
    ax.fill_between(time, y-1.96*err, y+1.96*err, alpha=0.2, color=stimtype_colormap[stim])

bottom = ax.get_ylim()[0]
delta  = np.diff(ax.get_yticks())[0] / 5
for i, stim in enumerate(stim_types):
    signif = L[stim]['p_val_adj'] > p_val_threshold
    height = bottom+delta*i
    signif_line =  np.ma.masked_where(signif, np.repeat(height, time.shape[0]))
    ax.plot(time, signif_line, color=stimtype_colormap[stim], lw=2)
height = bottom+delta*(i+1)
signif_diff = np.ma.masked_where(~reject, np.repeat(height, time.shape[0]))
ax.plot(time, signif_diff, color='k', lw=2, alpha=0.8)

signif_diff = np.array(z_statistic) > norm.ppf((1-p_val_threshold/2))

add_stim_to_plot(ax)
add_stimtype_legend(ax, stim_types)
add_time(ax, time)
sns.despine(fig=f, top=True, right=True)
ax.axhline(0, ls='--', c='k', alpha=0.2)
ax.set_ylabel('Area coefficient\n({})'.format(score_labels[score]))
plt.tight_layout()

if save_plots:
    plot_name ='plotLME_{}_response_on_raw_traces_{}{}_{}_n{}_s{}_r{}.{}'.format(method,
                debias_string, algorithm,score, n_sample, stim, n_repeats, plot_format)
    f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)

