For the decoding/clustering analysis:

- Run `clustering_on_raw_traces.py` and `decoding_on_raw_traces.py`
to perform the actual decoding/clustering, and save to `.pkl`.

- Run `run_lme.py` to apply the linear mixed effects model. This file
first load the pickled output of the decoding/clustering, then
it saves a manipulated dataframe to `.csv` (called `lme_dec_data*` or
`lme_clust_data*`)
and then it calls the R script which effectively computes the
mixed effects model for all frames and it saves the results of that
to `.csv` (`lme_dec_results*` or `lme_clust_results*`).
In `run_lme.py` stimulus types.

- Run `plot_lme_coefficient.py` to plot the results of the previous
step.



- `run_dec_clust_all.sh` runs the full decoding and clustering on the cluster
   for all algorithms / scores / group sizes.
- `run_lme_all.py` runs the lme routing for all algorithms / scores / group sizes.
- `plot_dec_clust_over_time_all.py` plots the decoding and clustering traces
   for all algorithms / scores / group sizes.
- `plot_lme_coefficient_all.py` plots the LME area coefficient per
   algorithms / scores / group sizes and also across group sizes per
   algorithms / scores.

