import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
import pickle
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from starecase.constants import *
from sklearn.cluster import KMeans, MiniBatchKMeans, DBSCAN
from sklearn.metrics import adjusted_rand_score, mutual_info_score
from sklearn.metrics import adjusted_mutual_info_score
from sklearn.metrics import normalized_mutual_info_score
from sklearn.metrics import homogeneity_completeness_v_measure
from sklearn.metrics import fowlkes_mallows_score
import argparse

home         = os.path.expanduser('~')
data_dir     = os.path.join(home,'data/STARECASE/preprocessed_longfinal/')
results_dir  = os.path.join(home, 'data/STARECASE/results_starecase/clustering/')

align_event  = 'stim_on'

decode_var   = 'response'
n_clusters   = 2
pos_label    = 0
resample     = 'neurons'
reduce_dim   = False


try:
    __IPYTHON__
    stim_types = [1]
    n_sample = 10
    n_repeats = 50
    algorithm = 'MiniBatchKMeans'

except NameError:
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n_sample', type=int)
    parser.add_argument('-r', '--n_repeats', type=int)
    parser.add_argument('-s', '--stim_type', type=int)
    parser.add_argument('-a', '--algorithm', type=str, default='MiniBatchKMeans')
    parser.add_argument('-d', '--reduce_dim', type=int, default=0)
    ARGS = parser.parse_args()
    n_sample = ARGS.n_sample
    n_repeats = ARGS.n_repeats
    stim_types = [ARGS.stim_type]
    algorithm = ARGS.algorithm
    reduce_dim = bool(ARGS.reduce_dim)


pars = {'decoding_alg' : algorithm,
        'decode_var'   : decode_var,
        'resample'     : resample,
        'stim_types'   : stim_types,
        'n_sample'     : n_sample,
        'n_repeats'    : n_repeats,
        'data_dir'     : data_dir,
        'results_dir'  : results_dir,
        'reduce_dim'   : reduce_dim}

print('RUNNING DECODING ROUTINE\n')
for k, v in pars.items():
    print('{!s:>15} : {!s:<25}'.format(k, v))
print('\n')

score_names = ['rand', 'mi', 'ami', 'nmi', 'h', 'c', 'v', 'fm']
C ={score: {m_id : {} for m_id in mouse_ids} for score in score_names}
#C ={m_id : {} for m_id in mouse_ids}


for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area,
                                          align_event)

    data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types)), :]
    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trials = [trl_data[n] for n in selected_trials]

    if decode_var == 'response':
        var = trl_info_df.loc[:, 'response'].as_matrix()
    elif decode_var == 'rt':
        var = trl_info_df.loc[:, 'rt'].as_matrix()
    y = np.array(var)

    trial_size = trials[0].shape[1]

    for score in score_names:
        C[score][mouse_id][area] = np.zeros([n_repeats, trial_size])

    for b in range(trial_size):
        X = np.hstack([tr[:, None, b] for tr in trials]).T

        for rep in range(n_repeats):

            if resample == 'neurons':
                resample_ind = np.random.choice(np.arange(X.shape[1]),
                                                size=n_sample,
                                                replace=False)
                Xres = X[:, resample_ind]
                yres = y
            else:
                Xres = X

            if reduce_dim:
                Xres = PCA(n_components=8).fit_transform(Xres)

            if algorithm == 'KMeans':
                clust = KMeans(n_clusters=n_clusters, init='k++').fit(Xres)
            elif algorithm == 'DBSCAN':
                clust = DBSCAN(eps=5, min_samples=8).fit(Xres)
            elif algorithm == 'MiniBatchKMeans':
                clust = MiniBatchKMeans(n_clusters=n_clusters).fit(Xres)

            y_pred = clust.labels_

            rand = adjusted_rand_score(y, y_pred)
            mi = mutual_info_score(y, y_pred)
            ami = adjusted_mutual_info_score(y, y_pred)
            nmi = normalized_mutual_info_score(y, y_pred)
            h, c, v = homogeneity_completeness_v_measure(y, y_pred)
            fm = fowlkes_mallows_score(y, y_pred)

            C['rand'][mouse_id][area][rep, b] = rand
            C['mi'][mouse_id][area][rep, b] = mi
            C['ami'][mouse_id][area][rep, b] = ami
            C['nmi'][mouse_id][area][rep, b] = nmi
            C['h'][mouse_id][area][rep, b] = h
            C['c'][mouse_id][area][rep, b] = c
            C['v'][mouse_id][area][rep, b] = v
            C['fm'][mouse_id][area][rep, b] = fm



out = {'results' : C, 'pars' : pars}
st = ''.join([str(t) for t in stim_types])
res_name = 'clust_{}_on_raw_traces_{}_n{}_s{}_r{}_d{}.pkl'.format(decode_var,
                algorithm, n_sample, st, n_repeats, int(reduce_dim))
if not os.path.isdir(results_dir):
    os.makedirs(results_dir)
destination = os.path.join(results_dir, res_name)
print('Saving results to {}'.format(destination))
pickle.dump(out, open(destination, 'wb'))



