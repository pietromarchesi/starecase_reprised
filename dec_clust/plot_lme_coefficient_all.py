import subprocess

methods = ['dec', 'clust']

# scores  = {'dec'   : ['precisionP', 'recallP', 'fbetaP',
#                       'precisionN', 'recallN', 'fbetaN',
#                       'accuracy', 'auc'],
#            'clust' : ['rand', 'mi', 'ami', 'nmi',
#                       'h', 'c', 'v', 'fm']}

scores  = {'dec'   : ['fbetaN', 'auc'],
           'clust' : ['nmi', 'ami', 'v']}

algorithms = {'dec'   : ['naive_bayes', 'random_forest', 'logistic_regression'],
              'clust' : ['KMeans']}


sample_sizes = [30]
n_repeats = 300
#debias = 1

plot_format  = 'png'
dpi          = 600

# for n_sample in sample_sizes:
#     for method in methods:
#         for algorithm in algorithms[method]:
#             for score in scores[method]:
#                 for debias in [1]:
#
#                     print('\nMethod: {} \nAlgorithm: {} \nScore: {} '\
#                           '\n# sample: {} \n# repeats: {} \n# debiased : {}'.format(method, algorithm, score,
#                                                                   n_sample, n_repeats, bool(debias)))
#
#                     subprocess.call(['python',
#                                      './plot_lme_coefficient.py',
#                                      '--n_sample', str(n_sample),
#                                      '--n_repeats', str(n_repeats),
#                                      '--method', method,
#                                      '--algorithm', algorithm,
#                                      '--score', score,
#                                      '--debias', str(debias),
#                                      '--plot_format', plot_format,
#                                      '--big_font', str(1),
#                                      '--dpi', str(dpi)])

sample_sizes = [10, 20, 30, 40, 50]

for method in methods:
    for algorithm in algorithms[method]:
        for score in scores[method]:
            for debias in [1]:
                subprocess.call(['python',
                                 './plot_lme_coefficient_across_group_size.py',
                                 '--n_sample_all', ' '.join(str(i) for i in sample_sizes),
                                 '--n_repeats', str(n_repeats),
                                 '--method', method,
                                 '--algorithm', algorithm,
                                 '--score', score,
                                 '--debias', str(debias),
                                 '--plot_format', plot_format,
                                 '--big_font', str(1),
                                 '--dpi', str(dpi)])