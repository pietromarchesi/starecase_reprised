import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cluster import KMeans, MiniBatchKMeans, DBSCAN
from sklearn.metrics import adjusted_rand_score, mutual_info_score
from sklearn.metrics import adjusted_mutual_info_score
from sklearn.metrics import normalized_mutual_info_score
from sklearn.metrics import homogeneity_completeness_v_measure
from sklearn.metrics import fowlkes_mallows_score
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

def timer(start,end):
    hours, rem = divmod(end-start, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds)


def decode(Xres, yres, algorithm, n_folds, random_state, n_estimators=100):

    kfold = StratifiedKFold(n_splits=n_folds, random_state=random_state,
                            shuffle=True)

    pP, rP, fP, pN, rN, fN, a, u = [], [], [], [], [], [], [], []
    for train_index, test_index in kfold.split(Xres, yres):

        if algorithm == 'naive_bayes':
            clf = GaussianNB()
        elif algorithm == 'random_forest':
            clf = RandomForestClassifier(n_estimators=n_estimators)
        elif algorithm == 'kneighbors':
            clf = KNeighborsClassifier()
        elif algorithm == 'svm':
            clf = SVC()
        elif algorithm == 'logistic_regression':
            clf = LogisticRegression()

        X_train, X_test = Xres[train_index], Xres[test_index]
        y_train, y_test = yres[train_index], yres[test_index]

        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)

        scoresP = precision_recall_fscore_support(y_test, y_pred,
                                                  average='binary',
                                                  pos_label=1)

        scoresN = precision_recall_fscore_support(y_test, y_pred,
                                                  average='binary',
                                                  pos_label=0)
        acc = accuracy_score(y_test, y_pred)

        y_score = clf.predict_proba(X_test)[:, 1]
        auc = roc_auc_score(y_test, y_score)

        pP.append(scoresP[0])
        rP.append(scoresP[1])
        fP.append(scoresP[2])
        pN.append(scoresN[0])
        rN.append(scoresN[1])
        fN.append(scoresN[2])
        a.append(acc)
        u.append(auc)

    scores = {}
    scores['precisionP'] = np.mean(pP)
    scores['recallP'] = np.mean(rP)
    scores['fbetaP'] = np.mean(fP)
    scores['precisionN'] = np.mean(pN)
    scores['recallN'] = np.mean(rN)
    scores['fbetaN'] = np.mean(fN)
    scores['accuracy'] = np.mean(a)
    scores['auc'] = np.mean(u)

    return scores





def cluster(Xres, yres, algorithm, n_clusters):

    if algorithm == 'KMeans':
        clust = KMeans(n_clusters=n_clusters).fit(Xres)
    elif algorithm == 'DBSCAN':
        clust = DBSCAN(eps=5, min_samples=8).fit(Xres)
    elif algorithm == 'MiniBatchKMeans':
        clust = MiniBatchKMeans(n_clusters=n_clusters).fit(Xres)

    y_pred = clust.labels_

    rand = adjusted_rand_score(yres, y_pred)
    mi = mutual_info_score(yres, y_pred)
    ami = adjusted_mutual_info_score(yres, y_pred)
    nmi = normalized_mutual_info_score(yres, y_pred)
    h, c, v = homogeneity_completeness_v_measure(yres, y_pred)
    fm = fowlkes_mallows_score(yres, y_pred)

    scores = {}
    scores['rand'] = rand
    scores['mi']   = mi
    scores['ami']  = ami
    scores['nmi']  = nmi
    scores['h']    = h
    scores['c']    = c
    scores['v']    = v
    scores['fm']   = fm

    return scores
