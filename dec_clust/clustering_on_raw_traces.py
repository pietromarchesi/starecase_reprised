import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import pickle
import numpy as np
import pandas as pd
from constants import *
import distutils.util
from dec_clust.dec_clust_utils import cluster
import time
from dec_clust.dec_clust_utils import timer
from constants import data_root, results_root

import argparse

home         = os.path.expanduser('~')
data_dir     = os.path.join(data_root, 'preprocessed_longfinal_exclFA')
results_dir  = os.path.join(results_root, 'clustering_exclFA')

align_event  = 'stim_on'

decode_var   = 'response'
n_clusters   = 2
pos_label    = 0
resample     = 'neurons'


try:
    __IPYTHON__
    stim_types = [3]
    n_sample = 10
    n_repeats = 45
    algorithm = 'KMeans'
    debias = True

except NameError:
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n_sample', type=int)
    parser.add_argument('-r', '--n_repeats', type=int)
    parser.add_argument('-s', '--stim_type', type=int)
    parser.add_argument('-a', '--algorithm', type=str, default='KMeans')
    parser.add_argument('-d', '--debias', type=distutils.util.strtobool, default=0)
    ARGS = parser.parse_args()
    n_sample = ARGS.n_sample
    n_repeats = ARGS.n_repeats
    stim_types = [ARGS.stim_type]
    algorithm = ARGS.algorithm
    debias = ARGS.debias

pars = {'decoding_alg' : algorithm,
        'decode_var'   : decode_var,
        'resample'     : resample,
        'stim_types'   : stim_types,
        'n_sample'     : n_sample,
        'n_repeats'    : n_repeats,
        'data_dir'     : data_dir,
        'results_dir'  : results_dir}

print('RUNNING CLUSTERING ROUTINE\n')
for k, v in pars.items():
    print('{!s:>15} : {!s:<25}'.format(k, v))
print('\n')

if not os.path.isdir(results_dir):
    os.makedirs(results_dir)

score_names = ['rand', 'mi', 'ami', 'nmi', 'h', 'c', 'v', 'fm']

if debias:
    C_keys = score_names + ['{}_shuffled'.format(s) for s in score_names]
else:
    C_keys = score_names

C ={score: {m_id : {} for m_id in mouse_ids} for score in C_keys}
#C ={m_id : {} for m_id in mouse_ids}


for mouse_id, area in datasets:

    t1 = time.time()
    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area,
                                          align_event)

    data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']

    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types)), :]
    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trials = [trl_data[n] for n in selected_trials]

    if decode_var == 'response':
        var = trl_info_df.loc[:, 'response'].as_matrix()
    elif decode_var == 'rt':
        trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]
        rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
        trl_info_df['rt'] = rt_bins.cat.codes
        var = trl_info_df.loc[:, 'rt'].as_matrix()

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trials = [trl_data[n] for n in selected_trials]
    y = np.array(var)

    trial_size = trials[0].shape[1]

    for key in C_keys:
        C[key][mouse_id][area] = np.zeros([n_repeats, trial_size])

    for b in range(trial_size):
        X = np.hstack([tr[:, None, b] for tr in trials]).T

        for rep in range(n_repeats):


            if resample == 'neurons':
                resample_ind = np.random.choice(np.arange(X.shape[1]),
                                                size=n_sample,
                                                replace=False)
                Xres = X[:, resample_ind]
                yres = y
            else:
                Xres = X


            output_scores = cluster(Xres, yres, algorithm=algorithm,
                                    n_clusters=n_clusters)

            for score_name in score_names:
                C[score_name][mouse_id][area][rep, b] = output_scores[score_name]

            if debias:
                yres_shuffle = np.random.permutation(yres)
                output_scores = cluster(Xres, yres_shuffle, algorithm=algorithm,
                                        n_clusters=n_clusters)

                for score_name in score_names:
                    C['{}_shuffled'.format(score_name)][mouse_id][area][rep, b] = output_scores[score_name]

    t2 = time.time()
    print('---- Finished working on dataset {} {} in {}'.format(mouse_id, area,
                                                                timer(t1, t2)))


out = {'results' : C, 'pars' : pars}
st = ''.join([str(t) for t in stim_types])
res_name = 'clust_{}_on_raw_traces_{}_n{}_s{}_r{}.pkl'.format(decode_var,
                algorithm, n_sample, st, n_repeats)
destination = os.path.join(results_dir, res_name)
print('Saving results to {}'.format(destination))
pickle.dump(out, open(destination, 'wb'))



