import os
import pandas as pd
import scipy
from statsmodels.stats.multitest import multipletests
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import pickle
import matplotlib.pyplot as plt
import seaborn as sn
from constants import *
from constants import data_root, results_root
from starecase_plotting_style import *
import distutils.util

try:
    __IPYTHON__
except NameError:

    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('--n_sample', type=int, default=30)
    parser.add_argument('--n_repeats', type=int, default=3000)

    parser.add_argument('--method', type=str, choices=['clust', 'dec'])
    parser.add_argument('--algorithm', type=str, choices=['random_forest',
                                                          'naive_bayes',
                                                          'logistic_regression',
                                                          'MiniBatchKMeans',
                                                          'KMeans'])
    parser.add_argument('--score', type=str, choices=['precisionP', 'recallP', 'fbetaP',
                                                       'precisionN', 'recallN', 'fbetaN',
                                                       'accuracy', 'auc',
                                                       'rand', 'mi', 'ami', 'nmi',
                                                       'h', 'c', 'v', 'fm'])
    parser.add_argument('--decode_var', type=str, default='response')
    parser.add_argument('--average_repeats', type=distutils.util.strtobool, default=1)
    parser.add_argument('--debias', type=distutils.util.strtobool, default=1)
    parser.add_argument('--plot_shuffled', type=distutils.util.strtobool, default=1)
    parser.add_argument('--err_style', type=str, choices=['ci_band', 'unit_traces'])
    parser.add_argument('--ci_trace')
    parser.add_argument('--ci_diff')
    parser.add_argument('--save_plots', type=distutils.util.strtobool, default=1)
    parser.add_argument('--plot_format', type=str, default='png')
    parser.add_argument('--dpi', type=int, default=500)

    ARGS = parser.parse_args()

    n_sample        = ARGS.n_sample
    n_repeats       = ARGS.n_repeats
    method          = ARGS.method
    algorithm       = ARGS.algorithm
    score           = ARGS.score
    decode_var      = ARGS.decode_var
    average_repeats = ARGS.average_repeats
    debias          = ARGS.debias
    plot_shuffled   = ARGS.plot_shuffled
    err_style       = ARGS.err_style
    ci_trace        = ARGS.ci_trace
    ci_diff         = ARGS.ci_diff
    save_plots      = ARGS.save_plots
    plot_format     = ARGS.plot_format
    dpi             = ARGS.dpi


else:

    n_sample            = 50
    n_repeats           = 300
    method              = 'dec'    # 'clust' o 'dec'
    algorithm           = 'naive_bayes' # MiniBatchKMeans, #KMeans, #random_forest, #naive_bayes
    score               = 'fbetaN'
    decode_var          = 'response'
    average_repeats     = True
    debias              = True
    plot_shuffled       = False
    err_style           = 'ci_band'
    ci_trace            = 'stderr'#38.3
    ci_diff             = 68

    save_plots = True
    plot_format = 'svg'
    dpi = 1000



stim_types          = [1, 3]
smooth              = True
sigma               = 1
time                = np.arange(-12, 76)
subsample_for_cis   = True
subsample_for_cis_n = 300


print(results_root)
print(method)
print(algorithm)
plots_dir = os.path.join(results_root, 'plots', 'dec_clust', method, algorithm)


if not os.path.isdir(plots_dir):
    os.makedirs(plots_dir)

if method == 'dec':
    data_dir = os.path.join(results_root, 'decoding')
elif method == 'clust':
    data_dir = os.path.join(results_root, 'clustering')

if debias:
    debias_string = 'debiased_'
else:
    debias_string = ''


if plot_shuffled:
    sharey = False
    plot_shuff_string = 'wshuffled_'
else:
    sharey = True
    plot_shuff_string = ''


C = {s : None for s in stim_types}
for stim in stim_types:
    C_file = '{}_{}_on_raw_traces_{}_n{}_s{}_r{}.pkl'.format(method, decode_var,
                algorithm, n_sample, stim, n_repeats)

    # lme_file = 'lme_{}_results_{}_{}_n{}_s{}_r{}.csv'.format(method, algorithm,
    #             score, n_sample, stim, n_repeats)

    C_stim  = pickle.load(open(os.path.join(data_dir, C_file), 'rb'))['results']
    #L_stim  = pd.read_csv(os.path.join(data_dir, lme_file))
    C[stim] = C_stim
scores = [score for score in C[stim_types[0]].keys()]


C_a = {s : {'V1' : [], 'AL' : []} for s in stim_types}
C_m = {s : {'V1' : [], 'AL' : []} for s in stim_types}

for stim in stim_types:
    for area in areas:
        for m_id in mouse_ids:
            try:
                if average_repeats:
                    # x = np.mean(C[stim][score][m_id][area], axis=0)
                    # x2 = np.mean(C[stim]['{}_shuffled'.format(score)][m_id][area],axis=0)

                    x = C[stim][score][m_id][area]
                    x2 = C[stim]['{}_shuffled'.format(score)][m_id][area]
                    if debias:
                        x  = x - x2
                        x = np.mean(x, axis=0)
                    if smooth:
                        x = gaussian_filter1d(x, sigma=sigma)
                        x2 = gaussian_filter1d(x2, sigma=sigma)

                else:
                    x = C[stim][score][m_id][area]
                    x2 = C[stim]['{}_shuffled'.format(score)][m_id][area]

                    if debias:
                        x  = x - x2

                    if smooth:
                        x = gaussian_filter1d(x, sigma=sigma)
                        x2 = gaussian_filter1d(x2, sigma=sigma)

                    if subsample_for_cis:
                        x = x[0:subsample_for_cis_n, :]
                C_a[stim][area].append(x)
                C_m[stim][area].append(x2)

            except KeyError:
                pass

C_a = {s : {area : np.vstack(C_a[s][area]) for area in areas} for s in stim_types}

C_m = {s : {area : np.vstack(C_m[s][area]) for area in areas} for s in stim_types}


# --- PLOT METRIC OVER TIME SPLIT BY TRIAL TYPE WITH CI -----------------------


f,axes = plt.subplots(1, len(stim_types), figsize=[len(stim_types)*4, 4], sharey=sharey)


# for area in areas:
#     for i, stim in enumerate(stim_types):


f,axes = plt.subplots(1, len(stim_types), figsize=[len(stim_types)*4, 4], sharey=sharey)
for area in areas:
    for i, stim in enumerate(stim_types):
        axes[i].set_title(stimtype_labels[stim])

        if isinstance(ci_trace, (float, int)):
            sns.tsplot(ax=axes[i], data=C_a[stim][area],time=time,
                       color=area_colormap[area],err_style=err_style, ci=ci_trace)
            axes[i].set_title(stimtype_labels[stim])
            if plot_shuffled:
                sns.tsplot(ax=axes[i], data=C_m[stim][area],time=time,
                           color=desaturated_area_colormap[area],err_style=err_style, ci=ci_trace)
        elif ci_trace == 'stderr':
            y = C_a[stim][area].mean(axis=0)
            err = C_a[stim][area].std(axis=0) / np.sqrt(C_a[stim][area].shape[0])
            axes[i].plot(time, y, c=area_colormap[area])
            axes[i].fill_between(time, y-0.5*err, y+0.5*err, alpha=0.2,
                                 facecolors=area_colormap[area])

for ax in axes:
    add_stim_to_plot(ax)
    add_time(ax, time)
    sns.despine(fig=f, top=True, right=True)
    #ax.set_ylim([0, 0.04])
add_area_legend(axes[1], areas, loc='lower right')
if method == 'clust':
    axes[0].set_ylabel('Cluster quality ({})'.format(score_labels[score]))
elif method == 'dec':
    axes[0].set_ylabel('Decoding performance ({})'.format(score_labels[score]))
plt.tight_layout()

if save_plots:
    plot_name ='plotA_TRACE_{}_{}_on_raw_traces_{}_{}{}_{}n{}_r{}_ci{}.{}'.format(method,
                decode_var, algorithm, debias_string, score, plot_shuff_string,
                n_sample, n_repeats, ci_trace, plot_format)
    f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)




f,axes = plt.subplots(1, len(stim_types), figsize=[len(stim_types)*4, 4], sharey=sharey)
for area in areas:
    for i, stim in enumerate(stim_types):
        axes[i].set_title(stimtype_labels[stim])
        y = C_a[stim][area].mean(axis=0)
        err = C_a[stim][area].std(axis=0) / np.sqrt(C_a[stim][area].shape[0])
        axes[i].plot(time, y, c=area_colormap[area])
        axes[i].fill_between(time, y-0.5*err, y+0.5*err, alpha=0.2,
                             facecolors=area_colormap[area])


# --- PLOT AL - V1 DIFFERENCE PER STIMULUS TYPE -------------------------------



C_b = {s : {'V1' : [], 'AL' : []} for s in stim_types}
for stim in stim_types:
    for m_id in paired_mouse_ids:
        for area in areas:
            x = np.mean(C[stim][score][m_id][area], axis=0)

            if debias:
                x = C[stim][score][m_id][area]
                x2 = C[stim]['{}_shuffled'.format(score)][m_id][area]
                x = np.mean(x - x2, axis=0)
            if smooth:
                x = gaussian_filter1d(x, sigma=sigma)


            C_b[stim][area].append(x)

C_b = {s : {area : np.vstack(C_b[s][area]) for area in areas} for s in stim_types}

f, ax = plt.subplots(1, 1)
for stim in stim_types:

    x = C_b[stim]['AL'] - C_b[stim]['V1']
    if isinstance(ci_diff, (int, float)):
        sns.tsplot(ax=ax, data=x, time=time,
               color=stimtype_colormap[stim], err_style='ci_band', ci=ci_diff)
    elif ci_diff == 'stderr':
        y = x.mean(axis=0)
        err = x.std(axis=0) / np.sqrt(x.shape[0])
        ax.plot(time, y, c=stimtype_colormap[stim])
        ax.fill_between(time, y-0.5*err, y+0.5*err, alpha=0.2,
                             facecolors=stimtype_colormap[stim])
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
add_stimtype_legend(ax, stim_types)

if save_plots:

    plot_name ='plotB_DIFF_{}_{}_on_raw_traces_{}_{}{}_n{}_r{}_ci{}.{}'.format(method,
                decode_var, algorithm, debias_string, score, n_sample, n_repeats, ci_diff, plot_format)
    f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)


# # --- PLOT METRIC OVER TIME SPLIT BY TRIAL TYPE WITH STDERR -------------------
# if False:
#     # this is equal to setting ci=68 in the tsplot version
#     f,axes = plt.subplots(1, len(stim_types), figsize=[len(stim_types)*5, 5], sharey=True)
#     for area in areas:
#         for i, stim in enumerate(stim_types):
#             x = C_a[stim][area]
#             y = x.mean(axis=0)
#             stder = x.std(axis=0) / np.sqrt(x.shape[0])
#             axes[i].plot(time, y, c=area_colormap[area])
#             axes[i].fill_between(time, y - stder, y + stder, alpha=0.2,
#                             facecolor=area_colormap[area], edgecolor="")
#             axes[i].set_title(stimtype_labels[stim])
#     for ax in axes:
#         ax.set_xlim([time[0], time[-1]])
#         add_stim_to_plot(ax)
#         sns.despine(fig=f, top=True, right=True)
#     add_area_legend(axes[1], areas)
#
#     if save_plots:
#         plot_name ='plotA_{}_{}_on_raw_traces_{}_{}{}_n{}_r{}.{}'.format(method,
#                     decode_var, algorithm, score, n_sample, n_repeats, plot_format)
#         f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)
#
#
# # --- PLOT METRIC OVER TIME SPLIT BY AREA -------------------------------------
# if False:
#
#     f,axes = plt.subplots(1, 2, figsize=[10, 5], sharey=True)
#     for stim in stim_types:
#         sns.tsplot(ax=axes[0], data=C_a[stim]['V1'],time=time,
#                    color=stimtype_colormap[stim],err_style=err_style, ci=ci_trace)
#         sns.tsplot(ax=axes[1], data=C_a[stim]['AL'],time=time,
#                    color=stimtype_colormap[stim],err_style=err_style, ci=ci_trace)
#     for ax in axes:
#         add_stim_to_plot(ax)
#         sns.despine(fig=f, top=True, right=True)
#     add_stimtype_legend(axes[1], stim_types)
#     axes[0].set_title('V1')
#     axes[1].set_title('AL')
#
#     if save_plots:
#         plot_name ='plotC_{}_{}_on_raw_traces_{}_{}_n{}_s{}_r{}.{}'.format(method,
#                     decode_var, algorithm, score, n_sample, stim, n_repeats, plot_format)
#         f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)



import scipy.stats as st
cl = 38.3
cl = 60
prob = 1 - (1-cl/100)/2
st.norm.ppf(prob)