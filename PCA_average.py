# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from starecase_plotting_style import *
from constants import *
from starecase_utils import fit_PCA_on_PSTHS

save_dir      = '/home/pietro/data/STARECASE/preprocessed/'
mouse_id      = '38'
area          = 'AL'
align_event   = 'stim_on'

preprocess    = 'z_score'
filter_contr  = False
contrast_thr  = 'auto'
stim_types    = [3]
n_components  = 30

save_plots    = False
plot_format   = 'png'
dpi           = 800
select        = True

average_vars  = ['orientation', 'response']


# --- LOAD DATA and FIT PCA ---------------------------------------------------

file_name     = 'starecase_session_{}_{}_' \
              'alignedto_{}.pkl'.format(mouse_id, area,
                                                     align_event)

data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

out = fit_PCA_on_PSTHS(data, stim_types=stim_types, average_vars=average_vars,
                       preprocess=preprocess, n_bins_contrast=4,
                       n_bins_rt=2, n_components=n_components)

pca = out['pca']
Xp_av = out['projected_psth']
trial_type = out['trial_type']
trial_types = out['trial_type_psth']
projected_trials = out['projected_trials']
trial_size = out['projected_trials'][0].shape[1]
time = out['time']
gt_av = out['gt_av']

# --- PLOT --------------------------------------------------------------------

def add_stim_to_plot_aligned_to_stim(ax):
    start_stim  = 0
    end_stim    = 27
    shade_alpha = 0.2
    lines_alpha = 0.8
    ax.axvspan(start_stim, end_stim, alpha=shade_alpha,
               color='gray')
    ax.axvline(start_stim, alpha=lines_alpha, color='gray', ls='--')
    ax.axvline(end_stim, alpha=lines_alpha, color='gray', ls='--')



contrast_colormap = sns.color_palette("Blues", n_colors=4)

fig, axes = plt.subplots(3, 3, figsize=[10, 6], sharey='row', sharex='col')

# iterate over the first 9 components
for comp in range(9):
    ax = axes.flatten()[comp]
    for kk, type in enumerate(trial_types):
        # for every trial type, select the part of the component
        # which corresponds to that trial type:
        x = Xp_av[comp, kk * trial_size :(kk+1) * trial_size]
        label = '{}'.format(type)
        ax.plot(time, x, label=label, c=response_colormap[type[1]])

        # if np.isin(comp, [3]):
        #     ax.plot(time, x, label=label, c=response_colormap[type[1]])
        # elif np.isin(comp, [1, 2, 5]):
        #     ax.plot(time, x, label=label, c=orientation_colormap[type[0]])
        # else:
        #     #ax.plot(time, x, label=label, c=stimtype_colormap[type[2]])
        #     ax.plot(time, x, label=label, c=contrast_colormap[type[3]])

    add_stim_to_plot(ax)

sns.despine(fig=fig, right=True, top=True)
#axes[0, 0].legend()
axes[1, 0].set_ylabel('PCA components')
plt.tight_layout()



# --- EXPLAINED VARIANCE ------------------------------------------------------
f, ax = plt.subplots(1, 1, figsize=[3, 3])
ax.plot(pca.explained_variance_ratio_)
ax.set_ylabel('Exaplined variance ratio')
ax.set_xlabel('# components')
ax.set_xticks(range(n_components))
sns.despine(right=True, top=True)
plt.tight_layout()



# --- DISTANCE BETWEEN AVERAGE TRAJECTORIES -----------------------------------

n_components = 10
comparisons = []
for tt in trial_types:
    ttc = list(tt)
    if ttc[1] == 0:
        ttc[1] = 1
    elif ttc[1] == 1:
        ttc[1] = 0
    comparisons.append((tt, tuple(ttc)))
comparisons = [tuple(x) for x in set(map(frozenset, comparisons))]


# create a dictionary with the first n components of the average pca
pca_av = {t : None for t in trial_types}
for kk, type in enumerate(trial_types):
    x = Xp_av[0:n_components, kk * trial_size :(kk+1) * trial_size]
    pca_av[type] = x


f, ax = plt.subplots(1, 1)
for type1, type2 in comparisons:
    print(type1, type2)
    a = pca_av[type1]
    b = pca_av[type2]
    d = np.linalg.norm(a - b, axis=0)
    ax.plot(time, d, label='{} - {}'.format(type1, type2))
add_stim_to_plot(ax)

plt.tight_layout()
sns.despine(fig=f, right=True, top=True)
ax.legend()



# --- 3D PLOT OF AVERAGE PROJECTION -------------------------------------------

component_x = 0
component_y = 1
component_z = 2
zero = 10
sigma=4
stim_duration=27
size_time_dots=30
from scipy.ndimage.filters import gaussian_filter1d

fig = plt.figure(figsize=[7, 7])
ax = fig.add_subplot(1, 1, 1, projection='3d')

for kk, t_type in enumerate(trial_types):
    # for every trial type, select the part of the component
    # which corresponds to that trial type:
    x = Xp_av[component_x, kk * trial_size:(kk + 1) * trial_size]
    y = Xp_av[component_y, kk * trial_size:(kk + 1) * trial_size]
    z = Xp_av[component_z, kk * trial_size:(kk + 1) * trial_size]

    x = gaussian_filter1d(x, sigma=sigma)
    y = gaussian_filter1d(y, sigma=sigma)
    z = gaussian_filter1d(z, sigma=sigma)

    ax.plot(x, y, z, c=orientation_colormap[t_type[0]],
            ls=response_linestyle[t_type[1]])
    ax.scatter(x[zero], y[zero], z[zero],
               c=orientation_colormap[t_type[0]],
               s=size_time_dots)
    ax.scatter(x[zero+stim_duration], y[zero+stim_duration], z[zero+stim_duration],
               edgecolors=orientation_colormap[t_type[0]],facecolors='none',
               s=size_time_dots+10)
ax.grid(False)
#ax.view_init(elev=17, azim=-14)
ax.view_init(elev=17, azim=20)
ax.xaxis.set_alpha(0.0)
fig.tight_layout()


if save_plots:
    dir = './plots/starecase'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'PCA_trajectory_{}_{}.{}'.format(mouse_id, area, plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    fig.savefig(os.path.join(dir, plot_name), dpi=dpi)


# --- 3D PLOT OF INDIVIDUAL TRIALS --------------------------------------------
fig = plt.figure(figsize=[7, 7])
ax = fig.add_subplot(1, 1, 1, projection='3d')

T = list(zip(projected_trials, trial_type))
for trial, t_type in T[50:100]:
    # for every trial type, select the part of the component
    # which corresponds to that trial type:
    x = trial[component_x, :]
    y = trial[component_y, :]
    z = trial[component_z, :]

    x = gaussian_filter1d(x, sigma=sigma)
    y = gaussian_filter1d(y, sigma=sigma)
    z = gaussian_filter1d(z, sigma=sigma)

    ax.plot(x, y, z, c=orientation_colormap[t_type[0]],
            ls=response_linestyle[t_type[1]])
    ax.scatter(x[zero], y[zero], z[zero],
               c=orientation_colormap[t_type[0]],
               s=size_time_dots)
    ax.scatter(x[zero+stim_duration], y[zero+stim_duration], z[zero+stim_duration],
               edgecolors=orientation_colormap[t_type[0]],facecolors='none',
               s=size_time_dots+10)
ax.grid(False)
#ax.view_init(elev=17, azim=-14)
ax.view_init(elev=17, azim=20)
ax.xaxis.set_alpha(0.0)
fig.tight_layout()


# --- PROJECT INDIVIDUAL TRIALS -----------------------------------------------

tsplot_err_style = 'ci_band'
tsplot_ci=95
plot_events = ['CUE-ON', 'GO-ON', 'SR', 'OT', 'RW-ON']


f, axes = plt.subplots(3, 2, figsize=[7, 7], sharey='row', sharex='col')
try:
    n_subplots = axes.shape[0] * axes.shape[1]
except IndexError:
    n_subplots = axes.shape[0]
n_comp_to_plot = min(n_components, n_subplots)

for comp in range(n_comp_to_plot):
    for t_type in trial_types:
        ax = axes.flatten()[comp]
        x = np.vstack(gt_av[comp][t_type])
        sns.tsplot(x, time=time,
                   ax=ax,
                   err_style=tsplot_err_style,
                   ci=tsplot_ci,
                   color=orientation_colormap[t_type[0]],
                   ls=response_linestyle[t_type[1]])

    add_stim_to_plot(ax)

sns.despine(fig=f, right=True, top=True)
plt.tight_layout()

if save_plots:
    dir = './plots/starecase'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'PCA_single_trials_on_average_PCA_{}_{}.{}'.format(mouse_id, area, plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    f.savefig(os.path.join(dir, plot_name), dpi=dpi)