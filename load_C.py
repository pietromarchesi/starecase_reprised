import os
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import pickle
import matplotlib.pyplot as plt
import seaborn as sn
from starecase.constants import *
from starecase.starecase_plotting_style import *

file = 'Dec_response_accuracy_on_raw_traces_naive_bayes_n10_s3_d0.pkl'
results_dir = '/home/pietro/data/STARECASE/results_starecase/'

C = pickle.load(open(os.path.join(results_dir, file), 'rb'))

save_plots      = False
average_repeats = True
score           = 'recall'
err_style       = 'ci_band'
smooth          = True
sigma           = 1
time            = np.arange(-12, 76)

C_a = {'V1' : [], 'AL' : []}
for area in areas:
    for m_id in mouse_ids:
        try:
            if average_repeats:
                if smooth:
                    m = np.mean(C[score][m_id][area], axis=0)
                    x = gaussian_filter1d(m, sigma=sigma)
                else:
                    x = np.mean(C[score][m_id][area], axis=0)
                C_a[area].append(x)
            else:
                if smooth:
                    m = C[score][m_id][area]
                    x = gaussian_filter1d(m, sigma=sigma)
                else:
                    x = C[score][m_id][area]
                C_a[area].append(x)
        except KeyError:
            pass
C_a = {area : np.vstack(C_a[area]) for area in areas}


# --- PLOT DECODING METRIC OVER TIME ------------------------------------------
f,ax = plt.subplots(1, 1)
for area in areas:
    sns.tsplot(ax=ax, data=C_a[area],time=time,
               color=area_colormap[area],err_style=err_style, ci='sd')

add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D
custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])



# --- PAIRED ------------------------------------------------------------------

Cb = []
for mouse_id in paired_mouse_ids:

    if smooth:
        m1 = np.mean(C[score][mouse_id]['AL'], axis=0)
        m2 = np.mean(C[score][mouse_id]['V1'], axis=0)
        x1 = gaussian_filter1d(m1, sigma=sigma)
        x2 = gaussian_filter1d(m2, sigma=sigma)
        x = x1 - x2
        Cb.append(x)
Cb = np.vstack(Cb)

f, ax = plt.subplots(1, 1)
sns.tsplot(ax=ax, data=Cb, time=time, err_style='ci_band', ci=95)
sns.tsplot(ax=ax, data=Cb, time=time, err_style='unit_traces', ci=95)
ax.axhline(0, alpha=0.8, c='grey', ls='--')
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
ax.set_ylim([-0.2, ax.get_ylim()[1]])


# --- PAIRED 2 ----------------------------------------------------------------

Cb = {m_id : [] for m_id in paired_mouse_ids}

for mouse_id in paired_mouse_ids:
    if smooth:
        m1 = C[score][mouse_id]['AL']
        m2 = C[score][mouse_id]['V1']

        x = m1 - m2
        Cb[mouse_id].append(x)
Cb = {m_id : np.vstack(Cb[m_id]) for m_id in paired_mouse_ids}


f, ax = plt.subplots(1, 1)
for m_id in paired_mouse_ids:
    sns.tsplot(ax=ax, data=Cb[m_id], time=time, err_style='ci_band', ci=95)
ax.axhline(0, alpha=0.8, c='grey', ls='--')
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
ax.set_ylim([-0.2, ax.get_ylim()[1]])


# --- COHEN'S U3 --------------------------------------------------------------



def u3(a, b):
    n = a.shape[0]
    l = (a <= np.median(b)).sum()
    return l / n

def bootstrap_u3(a, b, n_boot=100):
    u_boot = np.zeros(n_boot)

    for i in range(n_boot):
        a_boot = np.random.choice(a, size=a.shape[0], replace=True)
        b_boot = np.random.choice(b, size=b.shape[0], replace=True)
        u_boot[i] = u3(a_boot, b_boot)
    return u_boot


n_boot = 100
Cb = {m_id : np.zeros([n_boot, time.shape[0]]) for m_id in paired_mouse_ids}

for mouse_id in paired_mouse_ids:

    m1 = C[score][mouse_id]['V1']
    m2 = C[score][mouse_id]['AL']

    for t in range(m1.shape[1]):

        x = bootstrap_u3(m1[:, t], m2[:, t], n_boot=n_boot)
        Cb[mouse_id][:, t] = x

for mouse_id in paired_mouse_ids:
    for i in range(Cb[mouse_id].shape[0]):
        Cb[mouse_id][i, :] = gaussian_filter1d(Cb[mouse_id][i, :], sigma=2)

pal = sns.color_palette('husl', n_colors=len(paired_mouse_ids))
f, ax = plt.subplots(1, 1)
for i, m_id in enumerate(paired_mouse_ids):
    sns.tsplot(ax=ax, data=Cb[m_id], time=time, err_style='ci_band', ci='sd',
               color=pal[i])
from matplotlib.lines import Line2D

custom_lines = [Line2D([0], [0], color=pal[i], lw=4) for i in range(len(paired_mouse_ids))]
ax.legend(custom_lines, paired_mouse_ids)

ax.axhline(0.5, alpha=0.8, c='grey', ls='--')
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
ax.set_yticks(np.arange(0, 1.1, 0.25))




