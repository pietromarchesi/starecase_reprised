import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
import pickle
import numpy as np
from starecase.constants import *
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
import argparse

home         = os.path.expanduser('~')
data_dir     = os.path.join(home,'data/STARECASE/preprocessed_longfinal/')
results_dir  = os.path.join(home, 'data/STARECASE/results_starecase/clustering/')

decode_var   = 'response'
debias       = False
n_folds      = 5
resample     = 'neurons'

try:
    __IPYTHON__
    stim_types = [1]
    n_sample   = 65
    n_repeats  = 5
    algorithm  = 'naive_bayes'

except NameError:
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n_sample', type=int)
    parser.add_argument('-r', '--n_repeats', type=int)
    parser.add_argument('-s', '--stim_type', type=int)
    parser.add_argument('-a', '--algorithm', type=str)
    ARGS = parser.parse_args()
    n_sample = ARGS.n_sample
    n_repeats = ARGS.n_repeats
    stim_types = [ARGS.stim_type]
    algorithm = ARGS.algorithm



print('RUNNING DECODING ROUTINE\n')
for k, v in pars.items():
    print('{!s:>15} : {!s:<25}'.format(k, v))
print('\n')


r = {'V1' : [], 'AL' : []}

for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_stim_on.pkl'.format(mouse_id, area)

    data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    # select stim_type!!!!!
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types)), :]
    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trials = [trl_data[n] for n in selected_trials]

    if decode_var == 'response':
        var = trl_info_df.loc[:, 'response'].as_matrix()
    elif decode_var == 'rt':
        var = trl_info_df.loc[:, 'rt'].as_matrix()
    y = np.array(var)

    ratio = y[y==0].shape[0] / y[y==1].shape[0]
    r[area].append(ratio)

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
f, ax = plt.subplots(1, 1)
sns.distplot(r['AL'], ax=ax, rug=True, label='AL')
sns.distplot(r['V1'], ax=ax, rug=True, label='V1')
ax.legend()

