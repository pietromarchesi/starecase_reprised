

path='/home/pietro/data/STARECASE/results_starecase/clustering/clust_response_on_raw_traces_MiniBatchKMeans_n40_s3_r100_d1.pkl'


import os
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import pickle
import matplotlib.pyplot as plt
import seaborn as sn
from starecase.constants import *
from starecase.starecase_plotting_style import *


C = pickle.load(open(path, 'rb'))['results']

save_plots      = False
average_repeats = True
score           = 'nmi'
err_style       = 'ci_band'
smooth          = True
sigma           = 1
time            = np.arange(-12, 76)

C_a = {'V1' : [], 'AL' : []}
for area in areas:
    for m_id in mouse_ids:
        try:
            if average_repeats:
                if smooth:
                    m = np.mean(C[score][m_id][area], axis=0)
                    x = gaussian_filter1d(m, sigma=sigma)
                else:
                    x = np.mean(C[score][m_id][area], axis=0)
                C_a[area].append(x)
            else:
                if smooth:
                    m = C[score][m_id][area]
                    x = gaussian_filter1d(m, sigma=sigma)
                else:
                    x = C[score][m_id][area]
                C_a[area].append(x)
        except KeyError:
            pass
C_a = {area : np.vstack(C_a[area]) for area in areas}


# --- PLOT DECODING METRIC OVER TIME ------------------------------------------
f,ax = plt.subplots(1, 1)
for area in areas:
    sns.tsplot(ax=ax, data=C_a[area],time=time,
               color=area_colormap[area],err_style=err_style, ci='sd')

add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D
custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])
