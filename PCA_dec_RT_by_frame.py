# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
import sklearn.metrics
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase_plotting_style import *
from constants import *
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC


save_dir        = '/media/pietro/bigdata/neuraldata/STARECASE/preprocessed/'

align_event     = 'stim_on'
preprocess      = 'z_score'
reduce_dim      = False
n_components    = 20
decoding_alg    = 'naive_bayes'
decode_var      = 'rt'
nf              = 10
filter_contr    = False
contrast_thr    = 'auto'
stim_types      = [1]
select_neurons  = 'none'
average_repeats = True
n_folds         = 3
n_repeats       = 5

save_plots      = True
dpi             = 800
plot_format     = 'png'

scorer = sklearn.metrics.make_scorer(sklearn.metrics.f1_score)

C = {m_id : {} for m_id in mouse_ids}

for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))
    # load data
    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area, align_event)
    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))
    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']


    # 1. filter trial type
    if decode_var == 'rt':
        trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]


    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df.stim_type, stim_types)), :]

    # 2. filter for contrast
    if filter_contr:
        if contrast_thr == 'auto':
            contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std()
        elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
            contrast_thr_val = contrast_thr
        trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont < contrast_thr_val),:]
        print('Filtered contrast at {}'.format(contrast_thr_val))

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trl_data = {n : trl_data[n] for n in selected_trials}

    rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
    trl_info_df['rt'] = rt_bins.cat.codes

    # 3. select neurons
    # if np.isin(select_neurons, neuron_sel_opt):
    #     neuron_ind = data['info'][select_neurons].astype(bool)
    #     trials = [trl_data[k][neuron_ind, :] for k in trl_data.keys()]
    # else:
    #     trials = [trl_data[k] for k in trl_data.keys()]
    trials = [trl_data[k] for k in trl_data.keys()]


    # DECODING
    n_trials = len(trials)
    trial_size = trials[0].shape[1]
    if decode_var == 'response':
        var = trl_info_df['response'].tolist()
    elif decode_var == 'rt':
        var = trl_info_df['rt'].tolist()
    true_labels = np.array(var)

    C[mouse_id][area] = np.zeros([n_repeats, trial_size])

    all_pred_labels = [] # grip true_labels
    dec_metrics = {'accuracy' : []}

    for b in range(nf, trial_size-nf):
        frames = range(b-nf, b+nf+1)
        X =np.hstack([trials[t][:, frames].mean(axis=1)[:, None] for t in range(n_trials)]).T
        #X = np.hstack([trials[t][:, None, b] for t in range(n_trials)]).T

        if preprocess == 'min_max':
            X = min_max_scale(X, neuron_axis=1)
        elif preprocess == 'z_score':
            X = z_score(X, neuron_axis=1)

        if reduce_dim:
            pca = PCA(n_components=n_components)
            X = pca.fit_transform(X)


        if decoding_alg == 'naive_bayes':
            clf = GaussianNB()
        elif decoding_alg == 'random_forest':
            clf = RandomForestClassifier(n_estimators=50)
        elif decoding_alg == 'kneighbors':
            clf = KNeighborsClassifier()
        elif decoding_alg == 'svm':
            clf = SVC(kernel="linear", C=0.025)

        for rep in range(n_repeats):
            kfold = sklearn.model_selection.StratifiedKFold(n_splits=n_folds,
                                                            random_state=rep,
                                                            shuffle=True)

            acc = cross_val_score(clf, X, true_labels,
                                  cv=kfold,
                                  scoring=scorer)

            acc_rnd = cross_val_score(clf, X, np.random.permutation(true_labels),
                                  cv=kfold,
                                  scoring=scorer)
            #print(acc)
            C[mouse_id][area][rep, b] = np.mean(acc) - np.mean(acc_rnd)




err_style = 'ci_band'

C_a = {'V1' : [], 'AL' : []}
for area in areas:
    for m_id in mouse_ids:
        try:
            if average_repeats:
                C_a[area].append(np.mean(C[m_id][area], axis=0))
            else:
                C_a[area].append(C[m_id][area])
        except KeyError:
            pass
C_a = {area : np.vstack(C_a[area]) for area in areas}


# f,ax = plt.subplots(1, 1)
# for mouse_id, area in datasets:
#     sns.tsplot(ax=ax, data=C[mouse_id][area],time=time,
#                color=area_colormap[area],err_style=err_style)



f,ax = plt.subplots(1, 1)
for area in areas:
    sns.tsplot(ax=ax, data=C_a[area][:, nf:-nf],time=time[nf:-nf],
               color=area_colormap[area],err_style=err_style)

add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D
custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])

if save_plots:
    dir = './plots/starecase'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'PCA_decoding_{}_stim{}_r{}_f{}.{}'.format(decoding_alg, st,
                                                           int(reduce_dim),
                                                           int(filter_contr),
                                                           plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    f.savefig(os.path.join(dir, plot_name), dpi=dpi)



a = np.array([[1, 2, 3, 4, 5, 6],
              [1, 2, 3, 4, 5, 6],
              [1, 2, 3, 4, 5, 6]])
a[:, list(range(3-2, 3+2))]