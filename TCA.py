# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import seaborn as sns
import numpy as np
import tensortools as tt
import pandas as pd
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *



mouse_id      = '38'
area          = 'V1'
save_dir      = '/home/pietro/data/STARECASE/preprocessed_long/'

align_event   = 'stim_on'

preprocess    = 'min_max'
save_plot     = False
rank          = 5
color         = 'response'


file_name     = 'starecase_session_{}_{}_' \
              'alignedto_{}.pkl'.format(mouse_id, area, align_event)

data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

trl_info_df = data['trl_info_df']
trl_data = data['trl_data']


# selected_df = trl_info_df.loc[(trl_info_df['orientation'] == 330) &
#                               (np.isin(trl_info_df['stim_type'], [3]) ),:]


selected_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], [3]) ),:]


# selected_df = trl_info_df.loc[(np.isin(trl_info_df.trial_type, [0, 1, 2, 3, 11, 22, 33]) ),:]


selected_trials = selected_df.loc[:, 'trial_number'].as_matrix()
# filter dictionary to select data from those trials
trials = [trl_data[n] for n in selected_trials]

X = np.hstack(trials)

n_neurons = trials[0].shape[0]
n_trials  = len(trials)
n_times   = trials[0].shape[1]


if preprocess == 'min_max':
    X = min_max_scale(X, neuron_axis=0)
elif preprocess == 'z_score':
    X = z_score(X, neuron_axis=0)

X = np.reshape(X, (n_neurons, n_times, n_trials), order='F')


fit_kw = {
    'ranks': range(1, 10),
    'replicates': 3,
    'p_holdout': .9,
    'nonneg':True,
    'options': {
        'tol': 1e-2
    }
}

# neuron factor appearance
bar_kw = {
    'color': 'k',
    'width': 1
}
# temporal factor appearance
line_kw = {
    'color': 'r',
    'linewidth': 2
}
# temporal factor appearance
scatter_kw = {
    'color': 'k',
    'alpha': 0.7,
    's': 20
}


model, info = tt.cp_direct(X, rank=rank, nonneg=True, options=dict(tol=1e-6))

# model[0] = pd.DataFrame(model[0]).sort_values(by=[4, 2, 1, 3],
#                                               ascending=False).as_matrix()

estimate = np.einsum('ir,jr,kr->ijk', *model)


color_orient = [orientation_colormap[ori] for ori in selected_df.orientation]
color_stim   = [stimtype_colormap[stim] for stim in selected_df.stim_type]
color_resp   = [response_colormap[r] for r in selected_df.response]


n = selected_df.visual_cont.unique().__len__()
pal = sns.light_palette(sns.xkcd_rgb["magenta"], n_colors=n)
cont_codes = np.array(selected_df.visual_cont.astype('category').cat.codes)
color_Vcont  = [pal[cont] for cont in cont_codes]
color_Vcont  = [sns.xkcd_rgb["greyish"] if cont_codes[j] == -1 else c
                for j, c in enumerate(color_Vcont)]

n = selected_df.audio_cont.unique().__len__()
pal = sns.light_palette(sns.xkcd_rgb["cerulean"], n_colors=n)
cont_codes = np.array(selected_df.audio_cont.astype('category').cat.codes)
color_Acont  = [pal[cont] for cont in cont_codes]
color_Acont  = [sns.xkcd_rgb["greyish"] if cont_codes[j] == -1 else c
                for j, c in enumerate(color_Acont)]



if color == 'orient':
    color_list = color_orient
elif color == 'stimulus':
    color_list = color_stim
elif color == 'response':
    color_list = color_resp
elif color == 'visual_cont':
    color_list = color_Vcont
elif color == 'audio_cont':
    color_list = color_Acont


kw = dict(figsize=(12, 15), bar_kw=bar_kw, line_kw=line_kw, scatter_kw=scatter_kw)
fig, axes, pl = tt.plot_factors(model, plots=['bar', 'line', 'scatter'],
                                ylim=['link', 'tight', 'tight'], **kw)
fig.tight_layout()

for i in range(axes.shape[0]):
    pl[i,2].set_color(color_list)
    #pl[i,2].set_sizes(selected_df.visual_cont*2)



fig.tight_layout()
if save_plot:
    fig.savefig('./plots/TCA_{}.eps'.format(), dpi=1000)


