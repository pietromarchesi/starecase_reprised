import numpy as np

import jpype
import numpy as np
from constants import jarLocation

if not jpype.isJVMStarted():
    print('Starting JVM with jar at {}'.format(jarLocation))
    jpype.startJVM(jpype.getDefaultJVMPath(), "-ea",
                   "-Djava.class.path=" + jarLocation)


def to_list(var):
    if isinstance(var, np.ndarray):
        if len(var.shape) == 2:
            var = var.flatten().tolist()
        elif len(var.shape) == 1:
            var = var.tolist()
    elif isinstance(var, list):
        pass
    return var


def check_continuous_variable(var):
    assert isinstance(var, np.ndarray)
    if len(var.shape) == 1:
        var = var[:, None]
    var = var.astype(float)
    return var



def compute_info(var1, var2, estimator, n_surr=10, conditional=False,
                 condvar=None, base1=None, base2=None, condbase=None,
                 kraskov=1):

    if conditional:
        assert condvar is not None, ('You must pass a conditional variable!')

    if estimator == 'discrete':
        assert base1 is not None, ('For discrete estimation, must pass the base'
                                   'of variable 1')
        assert base2 is not None, ('For discrete estimation, must pass the base'
                                   'of variable 2')

        base1 = int(base1)
        base2 = int(base2)

        if conditional:
            assert condbase is not None, ('For discrete estimation, must pass '
                                          'the base of the conditional variable')
            condbase = int(condbase)

    print('--- Computing information with {} estimator, {} surrogates\n'
          '------ conditional: {}\n'.format(estimator, n_surr, conditional))

    if not conditional and condvar is not None:
        raise ValueError('You passed a conditional variable but the conditional'
                         'parameter is set to False!')

    if estimator == 'discrete':

        var1  = to_list(var1)
        var2  = to_list(var2)

        var1 = jpype.JArray(jpype.JInt, 1)(var1)
        var2 = jpype.JArray(jpype.JInt, 1)(var2)

        if conditional:

            condvar  = to_list(condvar)
            condvar  = jpype.JArray(jpype.JInt, 1)(condvar)


        # DISCRETE MI
        if condvar is None:

            calc_class = jpype.JPackage(
             "infodynamics.measures.discrete").MutualInformationCalculatorDiscrete
            calc = calc_class(base1, base2, 0)
            calc.initialise()
            calc.addObservations(var1, var2)

        elif conditional:
            calc_class = jpype.JPackage(
                "infodynamics.measures.discrete").ConditionalMutualInformationCalculatorDiscrete
            calc = calc_class(base1, base2, condbase)
            calc.initialise()
            calc.addObservations(var1, var2, condvar)



    if estimator == 'kraskov':

        var1 = check_continuous_variable(var1)
        var2 = check_continuous_variable(var2)

        ndim1 = var1.shape[1]
        ndim2 = var2.shape[1]

        if conditional:
            condvar = check_continuous_variable(condvar)
            ndimcond = condvar.shape[1]

        if not conditional:
            if kraskov == 1:
                calc_class = jpype.JPackage(
                    "infodynamics.measures.continuous.kraskov").MutualInfoCalculatorMultiVariateKraskov1
            elif kraskov == 2:
                calc_class = jpype.JPackage(
                    "infodynamics.measures.continuous.kraskov").MutualInfoCalculatorMultiVariateKraskov2
            else:
                raise ValueError('kraskov parameter must be either 1 or 2')
            calc = calc_class()
            calc.initialise(ndim1, ndim2)
            calc.setObservations(var1, var2)

        elif conditional:
            if kraskov == 1:
                calc_class = jpype.JPackage(
                    "infodynamics.measures.continuous.kraskov").ConditionalMutualInfoCalculatorMultiVariateKraskov1
            elif kraskov == 2:
                calc_class = jpype.JPackage(
                    "infodynamics.measures.continuous.kraskov").ConditionalMutualInfoCalculatorMultiVariateKraskov2
            else:
                raise ValueError('kraskov parameter must be either 1 or 2')
            calc = calc_class()
            calc.initialise(ndim1, ndim2, ndimcond)
            calc.setObservations(var1, var2, condvar)


    elif estimator == 'mixed':

        var1 = check_continuous_variable(var1)
        ndim1 = var1.shape[1]

        var2  = to_list(var2)

        if conditional:
            condvar = check_continuous_variable(condvar)
            ndimcond = condvar.shape[1]

        if not conditional:
            calc_class = jpype.JPackage(
             "infodynamics.measures.mixed.kraskov").MutualInfoCalculatorMultiVariateWithDiscreteKraskov
            calc = calc_class()  # n var of continuous, alphabet size of discrete
            calc.initialise(ndim1, base2)
            calc.setObservations(var1, var2)

        elif conditional:
            calc_class = jpype.JPackage(
            "infodynamics.measures.mixed.kraskov").ConditionalMutualInfoCalculatorMultiVariateWithDiscreteKraskov

            calc = calc_class()

            calc.initialise(ndim1, base2, ndimcond)
            calc.setObservations(var1, var2, condvar)


    if (estimator == 'mixed') & conditional:
        dist = calc.computeSignificance(True, n_surr)
    else:
        dist = calc.computeSignificance(n_surr)

    surr_mean = dist.getMeanOfDistribution()
    observed  = dist.actualValue
    p_val     = dist.pValue

    return observed, surr_mean, p_val




def add_significance_label(df):

    critical_p = 0.05 / df.shape[0]

    if (df['mi_p_val'] <= critical_p) and (df['cmi_p_val'] > critical_p):
        label = 'sig_mi'
    elif (df['mi_p_val'] > critical_p) and (df['cmi_p_val'] <= critical_p):
        label = 'sig_cmi'
    elif (df['mi_p_val'] <= critical_p) and (df['cmi_p_val'] <= critical_p):
        label = 'sig_both'
    else:
        label = 'not_sig'
    return label




# from idtxl.estimators_jidt import JidtKraskovMI, JidtDiscreteMI
# from idtxl.estimators_jidt import JidtKraskovCMI, JidtDiscreteCMI
#
# def mi_single_neuron(var1, var2, estimator):
#     if estimator == 'kraskov':
#         jidt_mi = JidtKraskovMI(settings=None)
#     elif estimator == 'discrete':
#         var1 = var1[:, 0]
#         alph1 = max(var1)+1
#         alph2 = max(var2)+1
#         jidt_mi = JidtDiscreteMI(settings={'discretise_method' : 'none',
#                                            'alph1' : alph1,
#                                            'alph2' : alph2})
#     return jidt_mi.estimate(var1, var2)
#
#
# def cmi_single_neuron(var1, var2, cond_var, estimator):
#     if estimator == 'kraskov':
#         jidt_cmi = JidtKraskovCMI(settings=None)
#     elif estimator == 'discrete':
#         var1 = var1[:, 0]
#         alph1 = max(var1)+1
#         alph2 = max(var2)+1
#         alphc = max(cond_var)+1
#         jidt_cmi = JidtDiscreteCMI(settings={'discretise_method' : 'none',
#                                              'alph1' : alph1,
#                                              'alph2' : alph2,
#                                              'alphc' : alphc})
#     return jidt_cmi.estimate(var1, var2, cond_var)

