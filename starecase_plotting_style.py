import numpy as np
import seaborn as sns
from matplotlib.lines import Line2D

import matplotlib
matplotlib.rcParams.update({'font.size': 13})

shade_alpha = 0.2
lines_alpha = 0.8

oc = ["windows blue", "amber", "faded green", "greyish"]
op = sns.xkcd_palette(oc)
orientation_colormap = {90 : op[0], 210 : op[1], 330 : op[2], 999: op[3]}

area_colormap = {'V1' : np.array([156, 0, 93]) / 255,
                 'AL' : np.array((245, 145, 30)) / 255}

n = 30
desaturated_area_colormap = {'V1' : np.array([170, 124, 141]) / 255,
                             'AL' : np.array((214, 178, 149)) / 255}


desaturated_area_colormap = {'V1' : np.array([110, 110, 110]) / 255,
                             'AL' : np.array((180, 180, 180)) / 255}


stimtype_colormap = {0 : sns.xkcd_rgb["greyish"],
                     1 : np.array((0, 113, 186)) / 255,
                     2 : np.array((191, 39, 45)) / 255,
                     3 : np.array((102, 45, 143)) / 255,
                     11 : sns.xkcd_rgb["strawberry"],
                     22 : sns.xkcd_rgb["bright lavender"],
                     33 : sns.xkcd_rgb["bright green"]}

response_colormap = {0 : sns.xkcd_rgb["watermelon"],
                     1 : sns.xkcd_rgb["jade green"]}

response_linestyle = {0 : '--',
                      1 : '-'}

stimtype_labels = {0 : 'Blank',
                   1 : 'V',
                   2 : 'A',
                   3 : 'AV',
                   11 : 'Full V',
                   22 : 'Full A',
                   33 : 'Full AV'}

score_labels = {'auc' : 'AUC score',
                'recallN' : 'Recall',
                'recallP' : 'Recall (hit as positive class)',
                'precisionN' : 'Precision',
                'precisionP' : 'Precision (hit as positive class)',
                'fbetaN' : 'F1 score',
                'fbetaP' : 'F1 score (hit as positive class)',
                'accuracy' : 'Accuracy',
                'nmi' : 'NMI',
                'v' : 'V-measure',
                'ami' : 'AMI',
                'fm' : 'Fowkles-Mallows index',
                'rand' : 'Rand index',
                'h': 'Homogeneity',
                'c': 'Completeness',
                'mi' : 'Mutual Information'}


def add_time(ax, time):
    ticks = time[np.mod(time, 26) == 0]
    ax.set_xticks(ticks)
    ax.set_xticklabels(range(ticks.shape[0]))
    ax.set_xlabel('Time (s)')


def add_stimtype_legend(ax, stim_types):
    custom_lines = [Line2D([0], [0], color=stimtype_colormap[stim], lw=4) for
                    stim in stim_types]
    ax.legend(custom_lines, [stimtype_labels[stim] for stim in stim_types],
              frameon=False)

def add_area_legend(ax, areas, loc='upper right'):
    custom_lines = [Line2D([0], [0], color=area_colormap[area], lw=4) for
                    area in areas]
    ax.legend(custom_lines, areas,
              frameon=False, loc=loc)



def add_stim_to_plot(ax):
    start_stim  = 0
    end_stim    = 27
    ax.axvspan(start_stim, end_stim, alpha=shade_alpha,
               color='gray')
    #ax.axvline(start_stim, alpha=lines_alpha, color='gray', ls='--')
    #ax.axvline(end_stim, alpha=lines_alpha, color='gray', ls='--')
