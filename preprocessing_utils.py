import numpy as np
from sklearn.preprocessing import StandardScaler, MinMaxScaler

def z_score(X, neuron_axis=1):

    if neuron_axis == 1:
        X_mean = np.mean(X, axis=0)
        X_std  = np.std(X, axis=0)
        X_std[X_std == 0] = 1
        Xz = (X - X_mean) / X_std
    elif neuron_axis == 0:
        X_mean = np.mean(X, axis=1)[:, np.newaxis]
        X_std  = np.std(X, axis=1)[:, np.newaxis]
        X_std[X_std == 0] = 1
        Xz = (X - X_mean) / X_std
    return Xz


def min_max_scale(X, min=0, max=1, neuron_axis=1):

    mm = MinMaxScaler(feature_range=(min, max))

    if neuron_axis == 1:
        Xm = mm.fit_transform(X)
    elif neuron_axis == 0:
        Xm = mm.fit_transform(X.T).T
    return Xm


def z_score_tensor(X):
    # experimental function
    Xf = X.reshape(X.shape[0], X.shape[1]*X.shape[2], order='F')

    Xf_mean = np.mean(Xf, axis=1)[:, np.newaxis]
    Xf_std = np.std(Xf, axis=1)[:, np.newaxis]
    Xf_std[Xf_std == 0] = 1
    Xfz= (Xf - Xf_mean) / Xf_std
    Xz = Xfz.reshape(X.shape[0], X.shape[1], X.shape[2], order='F')

    return Xz


