import os
import pandas as pd
import numpy as np
from scipy.ndimage.filters import gaussian_filter1d
import pickle
import matplotlib.pyplot as plt
import seaborn as sn
from starecase.constants import *
from starecase.starecase_plotting_style import *

n_sample = 30
stim     = 2
score    = 'auc'

file = 'dec_response_on_raw_' \
       'traces_naive_bayes_n{}_s{}_r200_d0.pkl'.format(n_sample, stim)
# file = 'Clust_response_v_on_raw_' \
#        'traces_MiniBatchKMeans_n{}_s{}_r200_d0.pkl'.format(n_sample, stim)
results_dir = '/home/pietro/data/STARECASE/results_starecase/decoding/'
results_dir2 = '/home/pietro/data/STARECASE/results_starecase/'

C = pickle.load(open(os.path.join(results_dir, file), 'rb'))['results']

n_time = C['auc'][mouse_ids[0]]['AL'].shape[1]
n_samp = C['auc'][mouse_ids[0]]['AL'].shape[0]


for frame in range(n_time):

    df = pd.DataFrame(columns=['score', 'area', 'mouse_id'])
    for mouse_id, area in datasets:
        x = {'score' : C[score][mouse_id][area][:, frame],
             'area' : area,
             'mouse_id' : mouse_id}
        dfx = pd.DataFrame(x)
        df = df.append(dfx)

    file_name = 'dec_df_me_n{}_s{}_{}_f{}.csv'.format(n_sample, stim, score, frame)
    df.to_csv(os.path.join(results_dir2, 'mixed_effects', file_name))