# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase_plotting_style import *
from mpl_toolkits.mplot3d import Axes3D


mouse_id      = '38'
area          = 'V1'
save_dir      = '/home/pietro/data/STARECASE/preprocessed_longfinal/'
plots_dir    = '/home/pietro/data/STARECASE/results_starecase/plots/'
align_event   = 'stim_on'

preprocess    = 'z_score'
n_components  = 10
stim_types    = [1, 3]
filter_contr  = False
contrast_thr  = 'auto'

save_plots    = True
plot_format   = 'pdf'
dpi           = 1000

file_name     = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area, align_event)

data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

trl_info_df = data['trl_info_df']
trl_data = data['trl_data']
time = data['time']

# 1. select stim types
trl_info_df = trl_info_df.loc[(np.isin(trl_info_df.stim_type, stim_types) ),:]


# 2. filter for contrast
if filter_contr:
    if contrast_thr == 'auto':
        contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std()/2

    elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
        contrast_thr_val = contrast_thr
    trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont < contrast_thr_val), :]
    print('Filtered contrast at {}'.format(contrast_thr_val))


selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()

combined_trial_type = [(t['orientation'], t['response'], t['stim_type']) for i, t in trl_info_df.iterrows()]

trial_types = list(set(set(combined_trial_type)))
trial_size = trl_data[list(trl_data.keys())[0]].shape[1]

X = np.hstack([trl_data[n] for n in selected_trials])



print('\nPreprocessing PSTHS for PCA: {}\n'.format(preprocess))
if preprocess == 'min_max':
    X = min_max_scale(X, neuron_axis=0)
elif preprocess == 'z_score':
    X = z_score(X, neuron_axis=0)

pca_long = PCA(n_components=10)
Xp = pca_long.fit_transform(X.T).T

trials = [Xp[:, trial_size * i: trial_size * (i + 1)] for i, t in
          enumerate(combined_trial_type)]

gt = {comp : {t_type : [] for t_type in trial_types}
      for comp in range(n_components)}

for comp in range(n_components):
    for i, t_type in enumerate(combined_trial_type):
        t = Xp[comp, trial_size * i: trial_size * (i + 1)]
        gt[comp][t_type].append(t)




tsplot_err_style = 'ci_band'
tsplot_ci=95

if False:
    f, axes = plt.subplots(3, 3, figsize=[12, 8])
    try:
        n_subplots = axes.shape[0] * axes.shape[1]
    except IndexError:
        n_subplots = axes.shape[0]
    n_comp_to_plot = min(n_components, n_subplots)

    for comp in range(n_comp_to_plot):
        for t_type in trial_types:
            ax = axes.flatten()[comp]
            # if np.isin(comp, [0, 1]):
            #     cmap = response_colormap
            #     el = t_type[1]
            # elif np.isin(comp, [2, 4]):
            #     cmap = orientation_colormap
            #     el = t_type[0]
            # else:
            #     cmap = stimtype_colormap
            #     el = t_type[2]

            cmap = orientation_colormap
            x = np.vstack(gt[comp][t_type])
            sns.tsplot(x, time=time,
                       ax=ax,
                       err_style=tsplot_err_style,
                       ci=tsplot_ci,
                       color=cmap[t_type[0]],
                       ls=response_linestyle[t_type[1]])
        add_stim_to_plot(ax)
        sns.despine(fig=f, right=True, top=True)

    plt.tight_layout()





component_x = 0
component_y = 1
component_z = 2
zero = 10
sigma=4
stim_duration=27
size_time_dots=30
from scipy.ndimage.filters import gaussian_filter1d

fig = plt.figure(figsize=[8, 4])
ax1 = fig.add_subplot(1, 2, 1, projection='3d')
ax2 = fig.add_subplot(1, 2, 2, projection='3d')
axes = [ax1,ax2]

for kk, t_type in enumerate(trial_types):
    # for every trial type, select the part of the component
    # which corresponds to that trial type:
    x = np.vstack(gt[component_x][t_type]).mean(axis=0)
    y = np.vstack(gt[component_y][t_type]).mean(axis=0)
    z = np.vstack(gt[component_z][t_type]).mean(axis=0)

    x = gaussian_filter1d(x, sigma=sigma)
    y = gaussian_filter1d(y, sigma=sigma)
    z = gaussian_filter1d(z, sigma=sigma)

    if t_type[2] == 1:
        ax = axes[0]
    elif t_type[2] == 3:
        ax = axes[1]
    ax.plot(x, y, z, c=orientation_colormap[t_type[0]],
            ls=response_linestyle[t_type[1]])
    ax.scatter(x[zero], y[zero], z[zero],
               c=orientation_colormap[t_type[0]],
               s=size_time_dots)
    ax.scatter(x[zero+stim_duration], y[zero+stim_duration], z[zero+stim_duration],
               edgecolors=orientation_colormap[t_type[0]],facecolors='none',
               s=size_time_dots+10)
for ax in axes:
    ax.grid(False)
    #ax.view_init(elev=17, azim=-14)
    ax.view_init(elev=29, azim=66)
    ax.xaxis.set_alpha(0.0)
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False
    ax.xaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_edgecolor('w')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels([])
ax1.set_title('V')
ax2.set_title('AV')

plt.tight_layout()

if save_plots:
    plot_name = 'PCA_average_long_traj_{}_{}.{}'.format(
                 mouse_id, area, plot_format)
    fig.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)

