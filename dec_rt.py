import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(sys.path[0]))))
import pickle
import numpy as np
import pandas as pd
from constants import *
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import OneHotEncoder
from decoders import DenseNNClassifier
from sklearn.svm import SVC
import argparse

home         = os.path.expanduser('~')
data_dir     = os.path.join(home,'data/STARECASE/preprocessed_longstate/')
results_dir  = os.path.join(home, 'data/STARECASE/results_starecase/decoding/')

n_folds      = 5
resample     = 'none'

try:
    __IPYTHON__
    stim_types = [1]
    n_sample   = 50
    n_repeats  = 5
    algorithm  = 'ffnn'
    decode_var = 'rt'

except NameError:
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--n_sample', type=int)
    parser.add_argument('-r', '--n_repeats', type=int)
    parser.add_argument('-s', '--stim_type', type=int)
    parser.add_argument('-a', '--algorithm', type=str, default='naive_bayes')
    parser.add_argument('-v', '--decode_var', type=str, default='response')
    ARGS = parser.parse_args()
    n_sample = ARGS.n_sample
    n_repeats = ARGS.n_repeats
    stim_types = [ARGS.stim_type]
    algorithm = ARGS.algorithm
    decode_var = ARGS.decode_var


pars = {'algorithm'    : algorithm,
        'decode_var'   : decode_var,
        'n_folds'      : n_folds,
        'resample'     : resample,
        'stim_types'   : stim_types,
        'n_sample'     : n_sample,
        'n_repeats'    : n_repeats,
        'data_dir'     : data_dir,
        'results_dir'  : results_dir,
        'decode_var'   : decode_var}

print('RUNNING DECODING ROUTINE\n')
for k, v in pars.items():
    print('{!s:>15} : {!s:<25}'.format(k, v))
print('\n')

score_names = ['precisionP', 'recallP', 'fbetaP',
               'precisionN', 'recallN', 'fbetaN', 'accuracy', 'auc']

C ={score: {m_id : {} for m_id in mouse_ids} for score in score_names}

for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_stim_on.pkl'.format(mouse_id, area)

    data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    # select stim_type!!!!!
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types)), :]

    if decode_var == 'response':
        var = trl_info_df.loc[:, 'response'].as_matrix()
    elif decode_var == 'rt':
        trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]
        rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
        trl_info_df['rt'] = rt_bins.cat.codes
        var = trl_info_df.loc[:, 'rt'].as_matrix()

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trials = [trl_data[n] for n in selected_trials]
    y = np.array(var)

    trial_size = trials[0].shape[1]

    for score in score_names:
        C[score][mouse_id][area] = np.zeros([n_repeats, trial_size])

    for b in range(trial_size):
        X = np.hstack([tr[:, None, b] for tr in trials]).T

        for rep in range(n_repeats):

            kfold = StratifiedKFold(n_splits=n_folds, random_state=rep,
                                    shuffle=True)

            if resample == 'trials':
                resample_ind = np.random.choice(np.arange(X.shape[0]),
                                                size=int(8 * (X.shape[0] / 10)),
                                                replace=False)
                Xres = X[resample_ind, :]
                yres = y[resample_ind]

            elif resample == 'neurons':
                resample_ind = np.random.choice(np.arange(X.shape[1]),
                                                size=n_sample,
                                                replace=False)
                Xres = X[:, resample_ind]
                yres = y

            else:
                Xres = X
                yres = y

            pP, rP, fP, pN, rN, fN, a, u = [], [], [], [], [], [], [], []
            for train_index, test_index in kfold.split(Xres, yres):

                X_train, X_test = Xres[train_index], Xres[test_index]
                y_train, y_test = yres[train_index], yres[test_index]

                if algorithm == 'naive_bayes':
                    clf = GaussianNB()
                elif algorithm == 'random_forest':
                    clf = RandomForestClassifier(n_estimators=100)
                elif algorithm == 'ffnn':
                    clf = DenseNNClassifier(units=[100, 100], dropout=0.3,
                                            num_epochs=20, verbose=0)
                    one_hot = OneHotEncoder(sparse=False)
                    y_train = one_hot.fit_transform(y_train.reshape(-1, 1)).astype(int)

                elif algorithm == 'kneighbors':
                    clf = KNeighborsClassifier()
                elif algorithm == 'svm':
                    clf = SVC()


                clf.fit(X_train, y_train)
                y_pred = clf.predict(X_test)
                if algorithm == 'ffnn':
                    y_pred = y_pred.argmax(axis=1)

                scoresP = precision_recall_fscore_support(y_test, y_pred,
                                                         average='binary',
                                                         pos_label=1)

                scoresN = precision_recall_fscore_support(y_test, y_pred,
                                                         average='binary',
                                                         pos_label=0)
                acc = accuracy_score(y_test, y_pred)

                if algorithm == 'ffnn':
                    y_score = clf.predict(X_test)[:, 1]
                else:
                    y_score = clf.predict_proba(X_test)[:, 1]
                auc = roc_auc_score(y_test, y_score)

                pP.append(scoresP[0])
                rP.append(scoresP[1])
                fP.append(scoresP[2])
                pN.append(scoresN[0])
                rN.append(scoresN[1])
                fN.append(scoresN[2])
                a.append(acc)
                u.append(auc)

            C['precisionP'][mouse_id][area][rep, b] = np.mean(pP)
            C['recallP'][mouse_id][area][rep, b] = np.mean(rP)
            C['fbetaP'][mouse_id][area][rep, b] = np.mean(fP)
            C['precisionN'][mouse_id][area][rep, b] = np.mean(pN)
            C['recallN'][mouse_id][area][rep, b] = np.mean(rN)
            C['fbetaN'][mouse_id][area][rep, b] = np.mean(fN)
            C['accuracy'][mouse_id][area][rep, b] = np.mean(a)
            C['auc'][mouse_id][area][rep, b] = np.mean(u)

            # if debias:
            #     ys = np.random.permutation(y)
            #     ps, rs, fs, acs = [], [], [], []
            #     for train_index, test_index in kfold.split(X, ys):
            #         X_train, X_test = X[train_index], X[test_index]
            #         y_train, y_test = ys[train_index], ys[test_index]
            #
            #         clf.fit(X_train, y_train)
            #         y_pred = clf.predict(X_test)
            #         scores = precision_recall_fscore_support(y_test, y_pred,
            #                                                  average='binary',
            #                                                  pos_label=pos_label)
            #         acc = accuracy_score(y_test, y_pred)
            #
            #         y_score = clf.predict_proba(X_test)[:, 1]
            #         auc = roc_auc_score(y_test, y_score)
            #
            #         ps.append(scores[0])
            #         rs.append(scores[1])
            #         fs.append(scores[2])
            #         acs.append(acc)
            #
            #     C['precision'][mouse_id][area][rep, b] = np.mean(p) - np.mean(ps)
            #     C['recall'][mouse_id][area][rep, b] = np.mean(r) - np.mean(rs)
            #     C['fbeta'][mouse_id][area][rep, b] = np.mean(f) - np.mean(fs)
            #     C['accuracy'][mouse_id][area][rep, b] = np.mean(a) - np.mean(acs)


out = {'results' : C, 'pars' : pars}
st = ''.join([str(t) for t in stim_types])
res_name = 'dec_{}_on_raw_traces_{}_n{}_s{}_r{}.pkl'.format(decode_var,
                algorithm, n_sample, st, n_repeats)
if not os.path.isdir(results_dir):
    os.makedirs(results_dir)
destination = os.path.join(results_dir, res_name)
print('Saving results to {}'.format(destination))
pickle.dump(out, open(destination, 'wb'))


