import os
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from starecase.constants import *
from starecase.starecase_plotting_style import *
from scipy.ndimage.filters import gaussian_filter1d
from starecase.starecase_utils import fit_PCA_on_PSTHS
from sklearn.cluster import KMeans, MiniBatchKMeans, DBSCAN
from sklearn.metrics import adjusted_rand_score, mutual_info_score
from sklearn.metrics import homogeneity_completeness_v_measure

save_dir     = '/home/pietro/data/STARECASE/preprocessed_longlong/'
align_event  = 'stim_on'
average_vars = ['response']
stim_types   = [3]
decoding_alg = 'naive_bayes'
target_var   = 'response'
debias       = True
n_components = 3
n_repeats    = 5
n_folds      = 5
pos_label    = 0
n_clusters   = 2
clustering_method = 'MiniBatchKMeans'

save_plots   = True
plot_format  = 'png'
dpi          = 500


score_names = ['rand', 'mi', 'h', 'c', 'v']
C ={score: {m_id : {} for m_id in mouse_ids} for score in score_names}
#C ={m_id : {} for m_id in mouse_ids}


for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area,
                                          align_event)

    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

    out = fit_PCA_on_PSTHS(data, stim_types=stim_types,
                          average_vars=average_vars, preprocess='z_score',
                          n_bins_contrast=2, n_bins_rt=2, n_components=n_components)

    if target_var == 'response':
        var = out['trl_info_df']['response'].tolist()
    elif target_var == 'rt':
        var = out['trl_info_df']['rt'].tolist()
    y = np.array(var)


    projected_trials = out['projected_trials']
    trial_size = projected_trials[0].shape[1]

    for score in score_names:
        C[score][mouse_id][area] = np.zeros([n_repeats, trial_size])

    for b in range(trial_size):
        X = np.hstack([tr[:, None, b] for tr in projected_trials]).T

        for rep in range(n_repeats):

            if clustering_method == 'KMeans':
                clust = KMeans(n_clusters=n_clusters).fit(X)
            elif clustering_method == 'DBSCAN':
                clust = DBSCAN(eps=5, min_samples=8).fit(X)
            elif clustering_method == 'MiniBatchKMeans':
                clust = MiniBatchKMeans(n_clusters=n_clusters).fit(X)

            y_pred = clust.labels_

            rand = adjusted_rand_score(y, y_pred)
            mi = mutual_info_score(y, y_pred)
            h, c, v  = homogeneity_completeness_v_measure(y, y_pred)


            C['rand'][mouse_id][area][rep, b] = rand
            C['mi'][mouse_id][area][rep, b] = mi
            C['h'][mouse_id][area][rep, b] = h
            C['c'][mouse_id][area][rep, b] = c
            C['v'][mouse_id][area][rep, b] = v



score           = 'mi'
average_repeats = True
err_style       = 'ci_band'
smooth          = True
sigma           = 2
time            = data['time']

C_a = {'V1' : [], 'AL' : []}
for area in areas:
    for m_id in mouse_ids:
        try:
            if smooth:
                m = np.mean(C[score][m_id][area], axis=0)
                x = gaussian_filter1d(m, sigma=sigma)
            else:
                x = np.mean(C[score][m_id][area], axis=0)
            C_a[area].append(x)
        except KeyError:
            pass
C_a = {area : np.vstack(C_a[area]) for area in areas}


# --- PLOT DECODING METRIC OVER TIME ------------------------------------------
f,ax = plt.subplots(1, 1)
for area in areas:
    sns.tsplot(ax=ax, data=C_a[area],time=time,
               color=area_colormap[area],err_style=err_style, ci=95)

add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D
custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])

if save_plots:
    dir = './plots/starecase'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'Clust_{}_{}_on_PCA_traces_{}_n{}_s{}_d{}.{}'.format(target_var,
                                                                   score,
                                                                   decoding_alg,
                                                                   n_components,
                                                                   st,
                                                                   int(debias),
                                                                   plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    f.savefig(os.path.join(dir, plot_name), dpi=dpi)




f,ax = plt.subplots(1, 1)
for mouse_id, area in datasets:
    sns.tsplot(ax=ax, data=C[score][mouse_id][area],time=time,
               color=area_colormap[area],err_style=err_style)