import os
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from starecase.constants import *
from starecase.starecase_plotting_style import *
from scipy.ndimage.filters import gaussian_filter1d
from starecase.starecase_utils import fit_PCA_on_PSTHS
from sklearn.metrics import make_scorer
from sklearn.metrics import accuracy_score, recall_score, f1_score, precision_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import StratifiedKFold
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

save_dir     = '/home/pietro/data/STARECASE/preprocessed_longfinal/'
align_event  = 'stim_on'
average_vars = ['stim_type', 'response', 'orientation']
stim_types   = [3]
decoding_alg = 'naive_bayes'
decode_var   = 'response'
debias       = False
n_components = 4
n_repeats    = 5
n_folds      = 5
pos_label    = 0


save_plots   = True
plot_format  = 'png'
dpi          = 500


score_names = ['precision', 'recall', 'fbeta', 'accuracy']
C ={score: {m_id : {} for m_id in mouse_ids} for score in score_names}
#C ={m_id : {} for m_id in mouse_ids}


for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area,
                                          align_event)

    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

    out = fit_PCA_on_PSTHS(data, stim_types=stim_types,
                          average_vars=average_vars, preprocess='min_max',
                          n_bins_contrast=2, n_bins_rt=2, n_components=n_components)

    if decode_var == 'response':
        var = out['trl_info_df']['response'].tolist()
    elif decode_var == 'rt':
        var = out['trl_info_df']['rt'].tolist()
    y = np.array(var)

    projected_trials = out['projected_trials']
    # trials = [data['trl_data'][t['trial_number']] for i, t in out['trl_info_df'].iterrows()]

    # for t1, t2 in zip(projected_trials, trials):
    #     np.testing.assert_array_equal(t1, t2)

    trial_size = projected_trials[0].shape[1]

    for score in score_names:
        C[score][mouse_id][area] = np.zeros([n_repeats, trial_size])

    for b in range(trial_size):
        X = np.hstack([tr[:, None, b] for tr in projected_trials]).T

        if decoding_alg == 'naive_bayes':
            clf = GaussianNB()
        elif decoding_alg == 'random_forest':
            clf = RandomForestClassifier(n_estimators=100)
        elif decoding_alg == 'kneighbors':
            clf = KNeighborsClassifier()
        elif decoding_alg == 'svm':
            clf = SVC()

        for rep in range(n_repeats):


            kfold = StratifiedKFold(n_splits=n_folds, random_state=rep,
                                    shuffle=True)


            p, r, f, a = [], [], [], []
            for train_index, test_index in kfold.split(X, y):
                X_train, X_test = X[train_index], X[test_index]
                y_train, y_test = y[train_index], y[test_index]

                clf.fit(X_train, y_train)
                y_pred = clf.predict(X_test)
                scores = precision_recall_fscore_support(y_test, y_pred,
                                                         average='binary',
                                                         pos_label=pos_label)
                acc = accuracy_score(y_test, y_pred)
                p.append(scores[0])
                r.append(scores[1])
                f.append(scores[2])
                a.append(acc)

            if debias:
                ys = np.random.permutation(y)
                ps, rs, fs, acs = [], [], [], []
                for train_index, test_index in kfold.split(X, ys):
                    X_train, X_test = X[train_index], X[test_index]
                    y_train, y_test = ys[train_index], ys[test_index]

                    clf.fit(X_train, y_train)
                    y_pred = clf.predict(X_test)
                    scores = precision_recall_fscore_support(y_test, y_pred,
                                                             average='binary',
                                                             pos_label=pos_label)
                    acc = accuracy_score(y_test, y_pred)
                    ps.append(scores[0])
                    rs.append(scores[1])
                    fs.append(scores[2])
                    acs.append(acc)

                C['precision'][mouse_id][area][rep, b] = np.mean(p) - np.mean(ps)
                C['recall'][mouse_id][area][rep, b] = np.mean(r) - np.mean(rs)
                C['fbeta'][mouse_id][area][rep, b] = np.mean(f) - np.mean(fs)
                C['accuracy'][mouse_id][area][rep, b] = np.mean(a) - np.mean(acs)

            else:
                C['precision'][mouse_id][area][rep, b] = np.mean(p)
                C['recall'][mouse_id][area][rep, b] = np.mean(r)
                C['fbeta'][mouse_id][area][rep, b] = np.mean(f)
                C['accuracy'][mouse_id][area][rep, b] = np.mean(a)



for average_repeats in [True]:
    score           = 'recall'
    err_style       = 'ci_band'
    smooth          = True
    sigma           = 1
    time            = data['time']

    C_a = {'V1' : [], 'AL' : []}
    for area in areas:
        for m_id in mouse_ids:
            try:
                if average_repeats:
                    if smooth:
                        m = np.mean(C[score][m_id][area], axis=0)
                        x = gaussian_filter1d(m, sigma=sigma)
                    else:
                        x = np.mean(C[score][m_id][area], axis=0)
                    C_a[area].append(x)
                else:
                    if smooth:
                        m = C[score][m_id][area]
                        x = gaussian_filter1d(m, sigma=sigma)
                    else:
                        x = C[score][m_id][area]
                    C_a[area].append(x)
            except KeyError:
                pass
    C_a = {area : np.vstack(C_a[area]) for area in areas}


    # --- PLOT DECODING METRIC OVER TIME ------------------------------------------
    f,ax = plt.subplots(1, 1)
    for area in areas:
        sns.tsplot(ax=ax, data=C_a[area],time=time,
                   color=area_colormap[area],err_style=err_style, ci=95)

    add_stim_to_plot(ax)
    sns.despine(fig=f, top=True, right=True)
    from matplotlib.lines import Line2D
    custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                    Line2D([0], [0], color=area_colormap['AL'], lw=4)]
    ax.legend(custom_lines, ['V1', 'AL'])

    if save_plots:

        dir = './plots/starecase'
        st = ''.join([str(t) for t in stim_types])
        plot_name = 'Dec_{}_{}_on_PCA_traces_{}_n{}_s{}_d{}_ar{}.{}'.format(decode_var,
                                                                    score,
                                                                    decoding_alg,
                                                                    n_components,
                                                                    st,
                                                                    int(debias),
                                                                    int(average_repeats),
                                                                    plot_format)
        if not os.path.isdir(dir):
            os.makedirs(dir)
        f.savefig(os.path.join(dir, plot_name), dpi=dpi)




f,ax = plt.subplots(1, 1)
for mouse_id, area in datasets:
    sns.tsplot(ax=ax, data=C[score][mouse_id][area],time=time,
               color=area_colormap[area],err_style='unit_traces')