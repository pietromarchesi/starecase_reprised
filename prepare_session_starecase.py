import os
import pickle
import pandas as pd
import numpy as np
import quantities as pq
from constants import *
from constants import data_root

dir           = os.path.join(data_root, 'raw_pickle_exclFA')
save_dir      = os.path.join(data_root, 'preprocessed_longfinal_exclFA')
# mouse_id      = '46'
# area          = 'V1'

align_event   = 'stim_on' #stim_on or lick
t_pre         = 12
t_post        = 76
sampling_rate = 25 * pq.Hz

# if t_post = 0: no data of the actual trial is included
# if t_post = 1: only the first frame of the trial is included
# if t_pre  = 5: 5 frames before stim. onset are included

if not os.path.isdir(save_dir):
    os.makedirs(save_dir)

for mouse_id, area in datasets:

    print('Preparing session {} {}'.format(mouse_id, area))

    file_name = 'starecase_{}_{}.pkl'.format(mouse_id, area)
    full_path = os.path.join(dir, file_name)
    dFoF = pq.UnitQuantity('dFoF', symbol='dFoF')
    D = pickle.load(open(full_path, 'rb'))

    df = D['df']
    info = D['info']
    # info['DPvav'] = np.logical_or(info['DPv'], info['DPav']).astype(int)

    stim_type_to_string = {0  : 'blank',
                            1  : 'V',
                            2  : 'A',
                            3  : 'AV',
                            11 : 'full V',
                            22 : 'full A',
                            33 : 'full AV'}

    trl_info_df = {'trial_number'      : [],
                'stim_type'           : [],
                'stim_type_string'    : [],
                'audio_cont'           : [],
                'visual_cont'          : [],
                'response'             : [],
                'orientation'          : [],
                'global_stim_on_frame' : [],
                'global_stim_off_frame': [],
                'global_lick_frame'    : [],
                'rel_stim_on_frame'    : [],
                'rel_stim_off_frame'   : [],
                'rel_lick_frame'       : [],
                'lick_time'            : [],
                'stim_on_time'         : [],
                'stim_off_time'        : [],
                't_start'              : [],
                't_stop'               : []}



    # --- GENERATE TRIAL INFO DATAFRAME -------------------------------------------
    '''
    Note that:
    1. global frame values indicate frames in the gloabal index which includes 
    all trials. These are used to extract trials from the global df.
    2. rel frame values indicate frame numbers relative to each trial.
    3. time values are always aligned to either stimulus onset or lick. 
    
    In the first loop below, we set up the trial info dataframe with frame and time
    value which are by default aligned to stimulus onset. 
    
    In the second for loop, we iterate over trials and align them using the
    user-input of t_pre and t_post and readjust all the relative frames and times. 
    '''


    for k, trial_n in enumerate(df.TrialNumber.unique()):

        if trial_n != -1:

            trial = df.loc[(df.TrialNumber == trial_n)]

            stim_type = trial.StimType.max()
            stim_type_string = stim_type_to_string[stim_type]
            trial_response = trial.Response.max()

            # if visual stimulus is present
            if np.isin(stim_type, [1, 3, 11, 33]):
                visual_contrast = trial.Visual.max()
            else:
                visual_contrast = np.nan

            # if audio stimulus is present
            if np.isin(stim_type, [2, 3, 22, 33]):
                audio_contrast = trial.Audio.max()
            else:
                audio_contrast = np.nan

            if trial_response == 1:
                lick_frame = np.array(trial.Lick).argmax()
            else:
                lick_frame = np.nan

            trl_info_df['trial_number'].append(trial.TrialNumber.max())
            trl_info_df['stim_type'].append(stim_type)
            trl_info_df['stim_type_string'].append(stim_type_string)
            trl_info_df['visual_cont'].append(visual_contrast)
            trl_info_df['audio_cont'].append(audio_contrast)
            trl_info_df['response'].append(trial_response)
            trl_info_df['orientation'].append(trial.Orientation.max())

            # frames in the global index used to slice the data
            trl_info_df['global_stim_on_frame'].append(trial.index[0])
            trl_info_df['global_stim_off_frame'].append(trial.index[-1])
            trl_info_df['global_lick_frame'].append(trial.index[0]+lick_frame)

            # frames relative to trial start
            trl_info_df['rel_stim_on_frame'].append(0)
            rel_stim_off_frame = trial.index[-1] - trial.index[0]
            trl_info_df['rel_stim_off_frame'].append(rel_stim_off_frame)
            trl_info_df['rel_lick_frame'].append(lick_frame)

            # times
            trl_info_df['lick_time'].append((lick_frame / sampling_rate).rescale(pq.s))
            trl_info_df['stim_on_time'].append(0*pq.s)
            trl_info_df['stim_off_time'].append((rel_stim_off_frame/ sampling_rate).rescale(pq.s))
            trl_info_df['t_start'].append(None)
            trl_info_df['t_stop'].append(None)



    trl_info_df = pd.DataFrame(trl_info_df)
    trl_info_df_old = trl_info_df.copy()



    trl_data = {}
    for index, trial_info in trl_info_df.iterrows():
        trial_number  = trial_info['trial_number']

        # get the global and relative frame of the alignment event
        if align_event == 'stim_on':
            event_frame      = trial_info['global_stim_on_frame']
            rel_event_frame  = trial_info['rel_stim_on_frame']

        elif align_event == 'lick':
            event_frame      = trial_info['global_lick_frame']
            rel_event_frame  = trial_info['rel_lick_frame']

        else:
            raise ValueError('No valid alignment event specified')

        stim_on_frame    = trial_info['rel_stim_on_frame'] - rel_event_frame + t_pre
        stim_off_frame   = trial_info['rel_stim_off_frame'] - rel_event_frame + t_pre
        lick_frame       = trial_info['rel_lick_frame'] - rel_event_frame + t_pre

        t_start          = (- t_pre / sampling_rate).rescale(pq.s)
        t_stop           = (t_post / sampling_rate).rescale(pq.s)
        stim_on_time     = ((stim_on_frame-t_pre) / sampling_rate).rescale(pq.s)
        stim_off_time    = ((stim_off_frame-t_pre)/ sampling_rate).rescale(pq.s)
        lick_time        = ((lick_frame-t_pre)/ sampling_rate).rescale(pq.s)

        trl_info_df.loc[index, 't_start']            = t_start
        trl_info_df.loc[index, 't_stop']             = t_stop
        trl_info_df.loc[index, 'lick_time']          = lick_time
        trl_info_df.loc[index, 'stim_on_time']       = stim_on_time
        trl_info_df.loc[index, 'stim_off_time']      = stim_off_time
        trl_info_df.loc[index, 'rel_lick_frame']     = lick_frame
        trl_info_df.loc[index, 'rel_stim_on_frame']  = stim_on_frame
        trl_info_df.loc[index, 'rel_stim_off_frame'] = stim_off_frame

        if not np.isnan(event_frame):
            t1 = event_frame - t_pre
            t2 = event_frame + t_post

            #trial_data = df.loc[(df.index >= t1) & (df.index < t2)]
            df_single_trial = df.loc[(df.index >= t1) & (df.index < t2),:]
            trial_n         = df_single_trial['TrialNumber']

            assert set(trial_n.unique()).issubset(set([-1, trial_number]))
            np.testing.assert_array_equal(np.array(df_single_trial.index),
                                          np.arange(t1, t2))

            data_single_trial = df_single_trial.loc[:, info['NeuronID']].as_matrix()

            trl_data[trial_number] = data_single_trial.T
        else:
            trl_info_df.drop(index, inplace=True, axis=0)


    data = {'trl_info_df' : trl_info_df,
            'trl_data' : trl_data,
            'time' : np.arange(-t_pre, t_post),
            'info' : info}

    #print(trl_info_df_old.loc[12])
    #print(trl_info_df.loc[12])

    # --- SAVE --------------------------------------------------------------------
    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area, align_event,
                                                       t_pre, t_post)

    pickle.dump(data, open(os.path.join(os.path.abspath(save_dir), file_name), 'wb'))