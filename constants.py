import os
if os.environ['USER'] == 'pietro':
    data_root = '/media/pietro/bigdata/neuraldata/STARECASE'
    results_root = '/media/pietro/bigdata/neuraldata/STARECASE/results_starecase'
elif os.environ['USER'] == 'pmarche1':
    data_root = '/data/pmarche1/STARECASE'
    results_root = '/data/pmarche1/STARECASE/results_starecase'

mouse_ids        = ['22', '23', '32', '38', '41', '42','43', '46', '48']
paired_mouse_ids = ['22', '23', '38', '41', '42', '48']

# mouse_ids        = ['23', '32', '38', '41', '42','43', '46', '48']
# paired_mouse_ids = ['23', '38', '41', '42', '48']


areas         = ['V1', 'AL']

datasets_string = ['22_AL',
                   '22_V1',
                   '23_AL',
                   '23_V1',
                   '32_V1',
                   '38_AL',
                   '38_V1',
                   '41_AL',
                   '41_V1',
                   '42_AL',
                   '42_V1',
                   '43_AL',
                   '46_V1',
                   '48_AL',
                   '48_V1']

paired_datasets_string = ['22_AL',
                   '22_V1',
                   '23_AL',
                   '23_V1',
                   '38_AL',
                   '38_V1',
                   '41_AL',
                   '41_V1',
                   '42_AL',
                   '42_V1',
                   '48_AL',
                   '48_V1']


datasets = [tuple(s.split('_')) for s in datasets_string]
paired_datasets = [tuple(s.split('_')) for s in paired_datasets_string]



neuron_sel_opt = ['NeuronSelect', 'ModFull', 'ModThreshold', 'DPv', 'DPav',
                  'DPvav']

