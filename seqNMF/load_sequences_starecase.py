import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(sys.path[0])))
import numpy as np
import matplotlib.pyplot as plt
from loadmat import loadmat
import pickle
import seaborn as sns
import quantities as pq
import argparse
from mpl_toolkits.mplot3d import Axes3D

data_dir    = '/home/pietro/data/STARECASE/preprocessed_long/'
mouse_id    = '38'
area        = 'V1'
align_event = 'stim_on'

single_trials_for_scatter = [20, 50, 100]

sequences_file   = '/home/pietro/data/STARECASE/sequences/seqNMF_runx1.mat'
R = loadmat(sequences_file)

input_data_file = '{}.pkl'.format(R['cfg']['input_file'].split(sep='.')[0])

file_name = 'starecase_session_{}_{}_' \
            'alignedto_{}.pkl'.format(mouse_id, area, align_event)
data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

trl_info_df = data['trl_info_df']
trl_data = data['trl_data']
time = data['time']


#N, T = input_data['trial'][0].shape

W = R['W']

n_components = W.shape[1]

if n_components == 4:
    H_plot_shape = (2, 2)
    H_plot_size = [10, 6]
elif n_components == 6:
    H_plot_shape = (2, 3)
    H_plot_size = [10, 6]
elif n_components == 9:
    H_plot_shape = (3, 3)
    H_plot_size = [10, 6]



sorted_ind_comp = W[:, 0, :].argmax(axis=1).argsort()




trial_types = ['PGHF', 'PGLF', 'SGHF', 'SGLF']
pal = sns.color_palette("husl", 5)
trial_style = {}
trial_style['PGHF'] = {'c' : pal[4], 'ls' : '-'}
trial_style['PGLF'] = {'c' : pal[4], 'ls' : '--'}
trial_style['SGHF'] = {'c' : pal[2], 'ls' : '-'}
trial_style['SGLF'] = {'c' : pal[2], 'ls' : '--'}



gt = {comp : {t_type : [] for t_type in trial_types}
      for comp in range(n_components)}

for comp in range(n_components):
    for i, t_type in enumerate(trial_type):
        t = H[comp, T * i: T * (i + 1)]
        gt[comp][t_type].append(t)


# --- PLOT SINGLE TRIAL RECONSTRUCTION ----------------------------------------




# --- PLOT W FACTORS ----------------------------------------------------------
with sns.axes_style("white",  {"axes.linewidth": ".6"}):


    f, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=[6, 4])
    for k in range(W.shape[1]):
        w = W[sorted_ind_comp, k, :]
        ax[k].imshow(w, aspect='auto', cmap='Greys')
        ax[k].set_xticks([])
        sns.despine(ax=ax[k], top=True, right=True)
    ax[0].set_ylabel('Neuron index')

    if save_plots:
        plot_name = 'seqNMF_run{}_factors.{}'.format(sequences_id, plots_format)

        f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)




# comp = 0
# f, ax = plt.subplots(1, 1)
# for i, trial in enumerate(input_data['trial']):
#     trial_type = input_data['trial_type'][i]
#     t = H[comp, T * i: T * (i + 1)]
#     ax.plot(t, **trial_line_style[trial_type], alpha=0.2)






tsplot_err_style = 'ci_band'
tsplot_ci=95

# --- PLOT H FACTORS ----------------------------------------------------------

f, axes = plt.subplots(*H_plot_shape, figsize=H_plot_size)
try:
    n_subplots = axes.shape[0] * axes.shape[1]
except IndexError:
    n_subplots = axes.shape[0]
n_comp_to_plot = min(n_components, n_subplots)

for comp in range(n_comp_to_plot):
    for t_type in trial_types:
        ax = axes.flatten()[comp]
        x = np.vstack(gt[comp][t_type])
        sns.tsplot(x, time=time,
                   ax=ax,
                   err_style=tsplot_err_style,
                   ci=tsplot_ci,
                   color=trial_style[t_type]['c'],
                   ls=trial_style[t_type]['ls'])

    ax.axvspan(np.nanmin(gos), np.nanmax(gos), alpha=shade_alpha, color='gray')
    ax.axvspan(np.nanmin(ots), np.nanmax(ots), alpha=shade_alpha, color='gray')
    ax.axvspan(np.nanmin(cos), np.nanmax(cos), alpha=shade_alpha, color='gray')
    ax.axvline(np.nanmean(gos), alpha=lines_alpha, color='gray', ls='--')
    ax.axvline(np.nanmean(ots), alpha=lines_alpha, color='gray', ls='--')
    ax.axvline(np.nanmean(cos), alpha=lines_alpha, color='gray', ls='--')
    ax.axvline(0, alpha=SR_line_alpha, color='gray', ls='--')
    sns.despine(fig=f, right=True, top=True)
    ax.set_xticks([np.mean(cos), np.mean(gos), 0, np.nanmean(ots)])
    ax.set_xticklabels(['CUE-ON', 'GO', 'SR', 'OT'])

plt.tight_layout()

if save_plots:
    plot_name = 'seqNMF_run{}_components.{}'.format(sequences_id, plots_format)

    f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)



# --- PLOT TRAJECTORIES -------------------------------------------------------

ind_cos = int((zero + int(np.mean(cos)))/time_factor)
ind_gos = int((zero + int(np.mean(gos))) / time_factor)
ind_sr = int(zero / time_factor)
ind_ots = int((zero + int(np.nanmean(ots))) / time_factor)

fig = plt.figure(figsize=[14, 7])
ax1 = fig.add_subplot(1, 3, 1, projection='3d')
ax2 = fig.add_subplot(1, 3, 2, projection='3d')
ax3 = fig.add_subplot(1, 3, 3, projection='3d')

axs = [ax1, ax2, ax3]

comps = [0, 2, 3]

for ax in axs:
    ax.axis('off')

    for t_type in trial_types:

        x = np.vstack(gt[comps[0]][t_type]).mean(axis=0)
        y = np.vstack(gt[comps[1]][t_type]).mean(axis=0)
        z = np.vstack(gt[comps[2]][t_type]).mean(axis=0)

        ax.plot(x, y, z,  label=t_type, **trial_style[t_type])

        for ind in [ind_cos, ind_gos, ind_sr, ind_ots]:

            ax.scatter(x[ind], y[ind], z[ind],
                       c=trial_style[t_type]['c'],
                       s=size_time_dots)
ax1.grid(False)
ax2.grid(False)
ax3.grid(False)
ax1.view_init(elev=164, azim=-36)
ax2.view_init(elev=91, azim=-82)
ax3.view_init(elev=8, azim=154) #or -9
plt.tight_layout()