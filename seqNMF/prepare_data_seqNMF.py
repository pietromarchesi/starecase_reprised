import os
import pickle
import numpy as np
import pandas as pd
import scipy.io
from starecase.constants import *

data_dir     = '/home/pietro/data/STARECASE/preprocessed_long/'
save_dir     = '/home/pietro/data/STARECASE/preprocessed_long_seqNMF/'
align_event  = 'stim_on'

stim_types   = [1]
orientations = [90, 330, 210]
responses    = [0, 1]


if not os.path.isdir(save_dir):
    os.makedirs(save_dir)

for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area,
                                          align_event)

    data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    trl_info_df = trl_info_df.loc[np.isin(trl_info_df['orientation'], orientations), :]
    trl_info_df = trl_info_df.loc[np.isin(trl_info_df['response'], responses), :]
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types) ),:]

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()

    rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
    trl_info_df['rt'] = rt_bins.cat.codes

    trl_data = {n : trl_data[n] for n in selected_trials}
    trials = [trl_data[t['trial_number']] for i, t in trl_info_df.iterrows()]


    X = np.hstack(trials)

    data = {}
    data['trial'] = X

    file_name_save = 'starecase_session_{}_{}_' \
                'alignedto_{}'.format(mouse_id, area,
                                          align_event)
    save_destination = os.path.join(save_dir, file_name_save)

    scipy.io.savemat(save_destination, mdict=data)
