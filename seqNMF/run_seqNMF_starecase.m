% DESCRIPTION: Demo code for running seqNMF on simulated and real data,
% including how to test significance of each factor on held-out data, and
% how to select lambda
% 
% ------------------------------------------------------------------------
% Andrew Bahle and Emily Mackevicius 1.26.2018
%
% See paper: 
% https://www.biorxiv.org/content/early/2018/03/02/273128
%clear 
%clc
addpath('/home/pietro/code/seqNMF')

path           = '/home/pietro/data/STARECASE/preprocessed_long_seqNMF/';

file_name      = strcat(path, 'starecase_session_38_V1_alignedto_stim_on.mat');

save_path      =  '/home/pietro/data/STARECASE/sequences/';
save_file      = 'seqNMF_runx1.mat';

load(file_name)

X = trial;

%% Fit with seqNMF
K = 20;
L = 10;
lambda =.001;
lambdaL1W = 0.002;
lambdaL1H = 0;
lambdaOrthoW = 0.1;
lambdaOrthoH = 0.5;
apply_scaling = 1;
useWupdate = 0; 

if apply_scaling
    display('min-max scaling input data');
    mindata = min(transpose(X));
    maxdata = max(transpose(X));
    Xmm = transpose(bsxfun(@rdivide, bsxfun(@minus, transpose(X), mindata), maxdata - mindata));
    X = Xmm;
end


shg; clf
display('Running seqNMF')
[W,H] = seqNMF(X,'K',K, 'L', L,'lambda', lambda, 'showPlot', 1, ...
               'tolerance', -Inf, ...
               'lambdaL1W', lambdaL1W, ...
               'lambdaL1H', lambdaL1H, ...
               'lambdaOrthoW', lambdaOrthoW, ...
               'lambdaOrthoH', lambdaOrthoH, ...
               'useWupdate', useWupdate);


%% Look at factors
%figure; SimpleWHPlot(W,H); title('SeqNMF reconstruction')
%figure; SimpleWHPlot(W,H,X); title('SeqNMF factors, with raw data')

%% save results
results = {};
results.W = W;
results.H = H;
results.X = X;
results.Xrec = helper.reconstruct(W, H);
results.cfg = {};
results.cfg.input_file = file_name;
results.cfg.config = config_name;
results.cfg.apply_scaling = apply_scaling;
results.cfg.K = K;
results.cfg.L = L;
results.cfg.lambdaL1W = lambdaL1W;
results.cfg.lambdaL1H = lambdaL1H;
results.cfg.lambdaOrthoW = lambdaOrthoW;
results.cfg.lambdaOrthoH = lambdaOrthoH;
results.cfg.lambda = lambda;
results.cfg.preprocessing_cfg = cfg;
save(strcat(save_path, save_file), '-struct', 'results')


% %% load example HVC calcium imaging data (from 6991FirstFewDaysForBatch)
% clear all
% display('Attempting to load MackeviciusData from seqNMF repository')
% load MackeviciusData
% display('loaded data')
% %% break data into training set and test set
% splitN = floor(size(NEURAL,2)*.75); 
% splitS = floor(size(SONG,2)*.75); 
% trainNEURAL = NEURAL(:,1:splitN); 
% trainSONG = SONG(:,1:splitS); 
% testNEURAL = NEURAL(:,(splitN+1):end); 
% testSONG = SONG(:,(splitS+1):end); 
% %% plot one example factorization
% rng(235); % fixed rng seed for reproduceability
% X = trainNEURAL;
% K = 10;
% L = 2/3; % units of seconds
% Lneural = ceil(L*VIDEOfs);
% Lsong = ceil(L*SONGfs);
% shg
% display('Running seqNMF on real neural data (from songbird HVC, recorded by Emily Mackevicius, Fee Lab)')
% [W, H, ~,loadings,power]= seqNMF(X,'K',K,'L',Lneural,...
%             'lambdaL1W', .1, 'lambda', .005, 'maxiter', 100, 'showPlot', 1,...
%             'lambdaOrthoW', 0); 
% p = .05; % desired p value for factors
% 
% display('Testing significance of factors on held-out data')
% [pvals,is_significant] = test_significance(testNEURAL,W,p);
% 
% W = W(:,is_significant,:); 
% H = H(is_significant,:); 
% 
% % plot, sorting neurons by latency within each factor
% [max_factor, L_sort, max_sort, hybrid] = helper.ClusterByFactor(W(:,:,:),1);
% indSort = hybrid(:,3);
% tstart = 180; % plot data starting at this timebin
% figure; WHPlot(W(indSort,:,:),H(:,tstart:end), X(indSort,tstart:end), ...
%     0,trainSONG(:,floor(tstart*SONGfs/VIDEOfs):end))
% title('Significant seqNMF factors, with raw data')
% figure; WHPlot(W(indSort,:,:),H(:,tstart:end), ...
%     helper.reconstruct(W(indSort,:,:),H(:,tstart:end)),...
%     0,trainSONG(:,floor(tstart*SONGfs/VIDEOfs):end))
% title('SeqNMF reconstruction')
% 
%% Procedure for choosing lambda
% nLambdas = 20; % increase if you're patient
% K = 10; 
% lambdas = sort([logspace(-1,-5,nLambdas)], 'ascend'); 
% loadings = [];
% regularization = []; 
% cost = []; 
% for li = 1:length(lambdas)
%     [N,T] = size(X);
%     [W, H, ~,loadings(li,:),power]= seqNMF(X,'K',K,'L',Lneural,...
%         'lambdaL1W', .1, 'lambda', lambdas(li), 'maxiter', 100, 'showPlot', 0); 
%     [cost(li),regularization(li),~] = helper.get_seqNMF_cost(X,W,H);
%     display(['Testing lambda ' num2str(li) '/' num2str(length(lambdas))])
% end
% %% plot costs as a function of lambda
% windowSize = 3; 
% b = (1/windowSize)*ones(1,windowSize);
% a = 1;
% Rs = filtfilt(b,a,regularization); 
% minRs = prctile(regularization,10); maxRs= prctile(regularization,90);
% Rs = (Rs-minRs)/(maxRs-minRs); 
% R = (regularization-minRs)/(maxRs-minRs); 
% Cs = filtfilt(b,a,cost); 
% minCs =  prctile(cost,10); maxCs =  prctile(cost,90); 
% Cs = (Cs -minCs)/(maxCs-minCs); 
% C = (cost -minCs)/(maxCs-minCs); 
% 
% clf; hold on
% plot(lambdas,Rs, 'b')
% plot(lambdas,Cs,'r')
% scatter(lambdas, R, 'b', 'markerfacecolor', 'flat');
% scatter(lambdas, C, 'r', 'markerfacecolor', 'flat');
% xlabel('Lambda'); ylabel('Cost (au)')
% set(legend('Correlation cost', 'Reconstruction cost'), 'Box', 'on')
% set(gca, 'xscale', 'log', 'ytick', [], 'color', 'none')
% set(gca,'color','none','tickdir','out','ticklength', [0.025, 0.025])

% 
% %% chose lambda=.005; run multiple times, see number of sig factors
% loadings = [];
% pvals = []; 
% is_significant = []; 
% X = trainNEURAL;
% nIter = 20; % increase if patient
% display('Running seqNMF multiple times for lambda=0.005')
% 
% for iteri = 1:nIter
%     [W, H, ~,loadings(iteri,:),power]= seqNMF(X,'K',K,'L',Lneural,...
%             'lambdaL1W', .1, 'lambda', .005, 'maxiter', 100, 'showPlot', 0); 
%     p = .05;
%     [pvals(iteri,:),is_significant(iteri,:)] = test_significance(testNEURAL,W,p);
%     W = W(:,is_significant(iteri,:)==1,:); 
%     H = H(is_significant(iteri,:)==1,:); 
%     [max_factor, L_sort, max_sort, hybrid] = helper.ClusterByFactor(W(:,:,:),1);
%     indSort = hybrid(:,3);
%     tstart = 300; 
%     clf; WHPlot(W(indSort,:,:),H(:,tstart:end), X(indSort,tstart:end), 0,trainSONG(:,floor(tstart*SONGfs/VIDEOfs):end))
%     display(['seqNMF run ' num2str(iteri) '/' num2str(nIter)])
% end
% figure; hold on
% h = histogram(sum(is_significant,2), 'edgecolor', 'w', 'facecolor', .7*[1 1 1]); 
% h.BinCounts = h.BinCounts/sum(h.BinCounts)*100; 
% xlim([0 10]); 
% xlabel('# significant factors')
% ylabel('% seqNMF runs')
% 
% %% Plot factor-triggered song examples and rastors
% addpath(genpath('misc_elm')); 
% figure; HTriggeredSpec(H,trainSONG,VIDEOfs,SONGfs,Lsong); 
% 
% figure; HTriggeredRaster(H,trainNEURAL(indSort,:),Lneural);
% 
