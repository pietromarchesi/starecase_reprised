# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *
from starecase.constants import *

save_dir      = '/home/pietro/data/STARECASE/preprocessed_longlong/'
align_event   = 'stim_on'

preprocess    = 'min_max'
filter_contr  = True
contrast_thr  = 'auto'
stim_types    = [1, 3]

save_plots    = True
plot_format   = 'png'
dpi           = 300
select        = True

C = {m_id : {ar : [] for ar in areas} for m_id in mouse_ids}

for mouse_id, area in datasets:

    file_name     = 'starecase_session_{}_{}_' \
                  'alignedto_{}.pkl'.format(mouse_id, area,
                                                         align_event)

    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types) ),:]

    # 2. filter for contrast
    if filter_contr:
        if contrast_thr == 'auto':
            contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std()/2

        elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
            contrast_thr_val = contrast_thr
        trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont < contrast_thr_val), :]
        print('Filtered contrast at {}'.format(contrast_thr_val))


    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    #contrast_bins = pd.qcut(trl_info_df['visual_cont'], q=4)
    #trl_info_df['contrast_bin'] = contrast_bins.cat.codes

    rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
    trl_info_df['rt'] = rt_bins.cat.codes

    trl_data = {n : trl_data[n] for n in selected_trials}

    combined_trial_type = [(t['orientation'], t['stim_type'],
                            t['rt']) for i, t in trl_info_df.iterrows()]
    trials = [trl_data[t['trial_number']] for i, t in trl_info_df.iterrows()]

    trial_types = list(set(set(combined_trial_type)))
    trial_size = trl_data[list(trl_data.keys())[0]].shape[1]
    trials_by_type = {k : [] for k in trial_types}

    for i, t in trl_info_df.iterrows():

        tn = t['trial_number']
        ty = (t['orientation'], t['stim_type'], t['rt'])

        trials_by_type[ty].append(trl_data[tn])

    av_psth = {ty: np.sum(trials_by_type[ty], axis=0) / len(trials_by_type[ty])
               for ty in trial_types}

    Xav = np.hstack([av_psth[t] for t in trial_types])

    Xav_mean = Xav.mean(axis=1)
    Xav_std = Xav.std(axis=1)
    Xav_std[Xav_std == 0] = 1
    print('\nPreprocessing PSTHS for PCA: {}\n'.format(preprocess))
    if preprocess == 'min_max':
        Xav = min_max_scale(Xav, neuron_axis=0)
    elif preprocess == 'z_score':
        Xav = z_score(Xav, neuron_axis=0)

    pca = PCA(n_components=15)
    Xp_av = pca.fit_transform(Xav.T).T

    n_components = 10
    comparisons = []
    for tt in trial_types:
        if tt[2] == 0:
            comparisons = comparisons + [(tt, t) for t in trial_types if
                                         t[2] == 1]
        elif tt[2] == 1:
            comparisons = comparisons + [(tt, t) for t in trial_types if
                                         t[2] == 0]

    comparisons = [tuple(x) for x in set(map(frozenset, comparisons))]
    comparisons.sort()

    # create a dictionary with the first n components of the average pca
    pca_av = {t: None for t in trial_types}
    for kk, type in enumerate(trial_types):
        x = Xp_av[0:n_components, kk * trial_size:(kk + 1) * trial_size]
        pca_av[type] = x

    for type1, type2 in comparisons:
        print(type1, type2)
        a = pca_av[type1]
        b = pca_av[type2]
        d = np.linalg.norm(a - b, axis=0)
        C[mouse_id][area].append(d)



# --- PLOT INDIVIDUAL ANIMALS -------------------------------------------------
err_style = 'ci_band'
f, ax = plt.subplots(1, 1)
for mouse_id, area in datasets:
    x = np.vstack(C[mouse_id][area])
    sns.tsplot(ax=ax, data=x, time=time,
               color=area_colormap[area], err_style=err_style)
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D

custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])



# --- PLOT ACROSS ANIMALS -----------------------------------------------------
err_style = 'ci_band'

Ca = {'V1' : [], 'AL' : []}
for mouse_id, area in datasets:
    for r in C[mouse_id][area]:
        Ca[area].append(r)

# Ca = {'V1' : [], 'AL' : []}
# for mouse_id, area in datasets:
#     Ca[area].append(np.vstack(C[mouse_id][area]).mean(axis=0))


f, ax = plt.subplots(1, 1)
for area in areas:
    sns.tsplot(ax=ax, data=np.vstack(Ca[area]), time=time,
               color=area_colormap[area], err_style=err_style)
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D
custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])

if save_plots:
    dir = './plots/starecase'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'PCA_traj_distance_RT_stim{}_n{}.{}'.format(st, n_components,
                                                 plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    f.savefig(os.path.join(dir, plot_name), dpi=dpi)
