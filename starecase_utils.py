import os
import pickle
import pandas as pd
import numpy as np
from preprocessing_utils import z_score, min_max_scale
from sklearn.decomposition import PCA

save_dir    = '/home/pietro/data/STARECASE/preprocessed/'
mouse_id    = '38'
area        = 'V1'
align_event = 'stim_on'

file_name = 'starecase_session_{}_{}_' \
            'alignedto_{}.pkl'.format(mouse_id, area,
                                      align_event)

data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))


trl_info_df = data['trl_info_df']
trl_data = data['trl_data']
time = data['time']


# -----------------------------------------------------------------------------
def fit_PCA_on_PSTHS(data, stim_types=None, orientations=None, responses=None,
                     average_vars=None, preprocess='z_score',
                     n_bins_contrast=2, n_bins_rt=2, n_components=10):

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    if average_vars is None:
        average_vars = ['orientation', 'response', 'stim_type']

    if stim_types is None:
        stim_types = [1]

    if orientations is None:
        orientations = [90, 210, 330]

    if responses is None:
        responses = [0, 1]

    # select orientation, stimulus type and responses
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['orientation'], orientations)), :]
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types)), :]
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['response'], responses)), :]

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()

    # add contrast or rt variables
    if np.isin('contrast', average_vars):
        contrast_bins = pd.qcut(trl_info_df['visual_cont'], q=n_bins_contrast)
        trl_info_df['contrast'] = contrast_bins.cat.codes
    if np.isin('rt', average_vars):
        trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]
        rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=n_bins_rt)
        trl_info_df['rt'] = rt_bins.cat.codes


    trl_data = {n: trl_data[n] for n in selected_trials}
    combined_trial_type = [tuple([t[k] for k in average_vars]) for i, t in
                           trl_info_df.iterrows()]
    trials = [trl_data[t['trial_number']] for i, t in trl_info_df.iterrows()]

    trial_types = list(set(set(combined_trial_type)))
    #trial_size = trl_data[list(trl_data.keys())[0]].shape[1]
    trials_by_type = {k: [] for k in trial_types}

    for i, t in trl_info_df.iterrows():
        tn = t['trial_number']
        ty = tuple([t[k] for k in average_vars])
        trials_by_type[ty].append(trl_data[tn])


    av_psth = {ty: np.sum(trials_by_type[ty], axis=0) / len(trials_by_type[ty])
               for ty in trial_types}
    Xav = np.hstack([av_psth[t] for t in trial_types])

    Xav_mean = Xav.mean(axis=1)
    Xav_std = Xav.std(axis=1)
    Xav_std[Xav_std == 0] = 1
    if preprocess == 'min_max':
        Xav = min_max_scale(Xav, neuron_axis=0)
    elif preprocess == 'z_score':
        Xav = z_score(Xav, neuron_axis=0)

    pca = PCA(n_components=n_components)
    Xp_av = pca.fit_transform(Xav.T).T

    # project trials on all components
    trials_projected = []
    for i, (t_type, trl) in enumerate(zip(combined_trial_type, trials)):
            centered_trial = (trl.T - Xav_mean) / Xav_std
            #t_pca_2 = np.matmul(centered_trial, pca.components_.T).T
            t_pca = pca.transform(centered_trial).T
            #np.testing.assert_array_almost_equal(t_pca, t_pca_2)
            trials_projected.append(t_pca)


    # project all single trials on individual components
    gt_av = {comp: {t_type: [] for t_type in trial_types}
             for comp in range(n_components)}
    for comp in range(n_components):
        for i, (t_type, trl) in enumerate(zip(combined_trial_type, trials)):
            centered_trial = (trl.T - Xav_mean) / Xav_std
            #t_pca_2 = np.matmul(centered_trial, pca.components_[comp, None].T).T
            t_pca = pca.transform(centered_trial).T[comp, None, :]
            #np.testing.assert_array_almost_equal(t_pca, t_pca_2)
            gt_av[comp][t_type].append(t_pca)


    out = {}
    out['pca'] = pca
    out['original_trials'] = trials
    out['projected_trials'] = trials_projected
    out['gt_av'] = gt_av
    out['projected_psth'] = Xp_av
    out['trial_type_psth'] = trial_types
    out['trial_type'] = combined_trial_type
    out['trl_info_df'] = trl_info_df
    out['time'] = time

    return out




"""
run_lme = function(data, score){
  p_val = c()
  coeff = c()
  stder = c()
  for(frame in 0:87){
    
    
    df = data[data$frame == frame, c('area', 'mouse_id', score)]
    df$area <- as.factor(df$area)
    df$mouse_id <- as.factor(df$mouse_id)
    names(df)[names(df) == score] = 'score'
    
    mod <- lmer(score ~ area + (1 | mouse_id), df)
    
    cf = summary(mod)$coefficients['areaV1', 'Estimate']
    coeff = c(coeff, cf)
    
    se = summary(mod)$coefficients['areaV1', 'Std. Error']
    stder = c(stder, se)
    #display(fm1)
    a = Anova(mod)
    p_val = c(p_val, a$`Pr(>Chisq)`)
  }
  
  p_val_adj = p.adjust(p_val, method = 'fdr')
  
  res = data.frame(coeff = coeff, 
                   stder = stder, 
                   p_val = p_val, 
                   p_val_adj = p_val_adj)
  
  return(res)
  }
"""