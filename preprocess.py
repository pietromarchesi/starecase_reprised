import os
import glob
import numpy as np
import pandas as pd
from loadmat import loadmat
import pickle
from constants import data_root

global mouse_IDs, n_recordings, all_paths, all_denoised_paths


all_paths = glob.glob(data_root + '/raw_traces_exclFA/*.mat')
destination = os.path.join(data_root, 'raw_pickle_exclFA')
#all_denoised_paths = glob.glob(glob_path + '/denoised/*.mat')

if not os.path.isdir(destination):
    os.makedirs(destination)

all_paths.sort()

# path = all_paths[1]
# rec = loadmat(path)['StareCase']

n_recordings = len(all_paths)
mouse_IDs    = list(set([f[-9:-7] for f in all_paths]))

def build_dataframe(path):
    mouse_id = path[-9:-7]
    area     = path[-6:-4]
    rec = loadmat(path)['StareCase']
    df = pd.DataFrame(rec['dFoF'], columns=rec['NeuronID'])

    for att in ['TrialNumber', 'Pupil', 'Response', 'StimType',
                'Visual', 'Audio', 'Lick', 'Orientation']:
        df[att] = rec[att]

    info = {}
    for att in ['NeuronID', 'SamplingFreq', 'ThresholdVis',
                'ThresholdAud', 'NeuronSelect']:
        info[att] = rec[att]

    # In hit trials, if the response occurs before 4 samples (at 25Hz, so
    # 100 ms) we consider it a chance lick and thus not an actual hit trial,
    # so we remove it.
    hit_trials = df.TrialNumber.loc[df.Response == 1].unique()
    for t in hit_trials:
        lick_frame = df[df.TrialNumber==t].Lick.argmax()\
                     -df[df.TrialNumber==t].Lick.index[0]
        if lick_frame < 4:
            df = df[df.TrialNumber != t]

    return df, mouse_id, area, info



for path in all_paths:

    df, mouse_id, area, info = build_dataframe(path)

    print('pickling animal {} area {}'.format(mouse_id, area))
    out = {'df' : df,
           'mouse_id' : mouse_id,
           'area' : area,
           'info' : info}

    file_name = 'starecase_{}_{}.pkl'.format(mouse_id, area)

    pickle.dump(out, open(os.path.join(destination, file_name), 'wb'))
