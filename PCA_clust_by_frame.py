# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *
from starecase.constants import *
from dmrd_utils import PCA_clustering_by_frame


save_dir      = './preprocessed/starecase'
align_event   = 'stim_on'

clustering_alg= 'MiniBatchKMeans'
save_plots    = False
n_components  = 15
n_clusters    = 2
plot_format   = '.svg'
dpi           = 500
preprocess    = 'min_max'
filter_contr  = False
contrast_thr  = 'auto'
stim_types    = [3]
select_neurons= 'none'

clust = {'rand': [], 'mi': [], 'homogeneity': [],
                 'completeness': [], 'V': [], 'FM' : []}
ar = []

C = {m_id : {ar : None for ar in areas} for m_id in mouse_ids}

for mouse_id, area in datasets:
    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area, align_event)

    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    # 1. filter trial type
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df.trial_type, stim_types)), :]

    # 2. filter for contrast
    if filter_contr:
        if contrast_thr == 'auto':
            contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std()
        elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
            contrast_thr_val = contrast_thr
        trl_info_df = trl_info_df.loc[
                      (trl_info_df.visual_cont < contrast_thr_val), :]
        print('Filtered contrast at {}'.format(contrast_thr_val))

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trl_data = {n: trl_data[n] for n in selected_trials}

    # 3. select neurons
    if np.isin(select_neurons, neuron_sel_opt):
        neuron_ind = data['info'][select_neurons].astype(bool)
        trials = [trl_data[k][neuron_ind, :] for k in trl_data.keys()]
    else:
        trials = [trl_data[k] for k in trl_data.keys()]

    response = trl_info_df['response'].tolist()
    true_labels = np.array(response)

    P, GL, clust_metrics = PCA_clustering_by_frame(trials, true_labels,
                                                   n_components=n_components,
                                                   preprocess=preprocess,
                                                   clustering_method=clustering_alg,
                                                   n_clusters=n_clusters)

    for metric in ['mi', 'rand', 'homogeneity', 'completeness', 'V', 'FM']:
        clust[metric].append(clust_metrics[metric])

    C[mouse_id][area] = clust_metrics

    ar.append(area)


metric = 'homogeneity'
clust_met = np.array(clust[metric])
err_style = 'ci_band'

f,ax = plt.subplots(1, 1)
for area in areas:
    sns.tsplot(ax=ax, data=clust_met[(np.array(ar) == area), :],
               time=time,
               color=area_colormap[area], err_style=err_style)

add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D
custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])


if save_plots:
    dir = os.path.join('./plots/orient', 'starecase')
    plot_name = 'PCA_clustering_{}.{}'.format(metric, plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    f.savefig(os.path.join(dir, plot_name), dpi=dpi)



f,ax = plt.subplots(1, 1)
for mouse_id in paired_mouse_ids:
    x = np.array(C[mouse_id]['AL'][metric]) - np.array(C[mouse_id]['V1'][metric])
    ax.plot(time, x, label=mouse_id)
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
ax.legend()
ax.axhline(0, ls='--', alpha=0.4)
plt.tight_layout()


