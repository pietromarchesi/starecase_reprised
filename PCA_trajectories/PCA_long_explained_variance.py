# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *
from starecase.constants import *

home = os.path.expanduser('~')
data_dir = os.path.join(home, 'data/STARECASE/preprocessed_longfinal/')
results_dir = os.path.join(home,
                           'data/STARECASE/results_starecase/PCA_trajectories/')

align_event = 'stim_on'

preprocess = 'z_score'
n_components = 30

filter_contr = False
contrast_thr = 'auto'
ev = []

for stim_types in [[1], [3]]:

    C = {m_id: {ar: [] for ar in areas} for m_id in mouse_ids}

    for mouse_id, area in datasets:

        print('\n\nWorking on dataset: {} {}, stim: {}'.format(mouse_id, area, stim_types))

        file_name = 'starecase_session_{}_{}_' \
                    'alignedto_{}.pkl'.format(mouse_id, area, align_event)

        data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

        trl_info_df = data['trl_info_df']
        trl_data = data['trl_data']
        time = data['time']

        # 1. select stim types
        trl_info_df = trl_info_df.loc[
                      (np.isin(trl_info_df.stim_type, stim_types)), :]

        # 2. filter for contrast
        if filter_contr:
            if contrast_thr == 'auto':
                contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std() / 2

            elif isinstance(contrast_thr, int) or isinstance(contrast_thr,
                                                             float):
                contrast_thr_val = contrast_thr
            trl_info_df = trl_info_df.loc[
                          (trl_info_df.visual_cont < contrast_thr_val), :]
            print('Filtered contrast at {}'.format(contrast_thr_val))

        selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()

        trial_type = [(t['response'], t['orientation']) for i, t in
                      trl_info_df.iterrows()]

        trial_types = list(set(set(trial_type)))
        trial_size = trl_data[list(trl_data.keys())[0]].shape[1]

        X = np.hstack([trl_data[n] for n in selected_trials])

        if preprocess == 'min_max':
            X = min_max_scale(X, neuron_axis=0)
        elif preprocess == 'z_score':
            X = z_score(X, neuron_axis=0)

        pca_long = PCA(n_components=n_components)
        Xp = pca_long.fit_transform(X.T).T

        ev.append(np.sum(pca_long.explained_variance_ratio_))