# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase_plotting_style import *
from constants import *



save_dir      = '/home/pietro/data/STARECASE/preprocessed_longfinal/'
#save_dir      = './preprocessed/starecase'

align_event   = 'stim_on'

preprocess    = 'z_score'
reduce_dim    = True
n_components  = 20
filter_contr  = True
contrast_thr  = 'auto'
stim_types    = [3]
select_neurons= 'none'
contrasts     = 'all' #'all' or 'paired'

save_plots    = False
dpi           = 500
plot_format   = '.png'



C = {m_id : {ar : [] for ar in areas} for m_id in mouse_ids}

for mouse_id, area in datasets:

    print('\n\nWorking on dataset: {} {}, stim: {}'.format(mouse_id, area,
                                                           stim_types))
    # load data
    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area, align_event)
    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    # 1. filter trial type
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df.stim_type, stim_types)), :]

    # 2. filter for contrast
    if filter_contr:
        if contrast_thr == 'auto':
            contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std()
        elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
            contrast_thr_val = contrast_thr
        trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont < contrast_thr_val),:]
        print('Filtered contrast at {}'.format(contrast_thr_val))

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
    trl_data = {n : trl_data[n] for n in selected_trials}

    # 3. select neurons
    if np.isin(select_neurons, neuron_sel_opt):
        neuron_ind = data['info'][select_neurons].astype(bool)
        trials = [trl_data[k][neuron_ind, :] for k in trl_data.keys()]
    else:
        trials = [trl_data[k] for k in trl_data.keys()]

    combined_trial_type = [(t['orientation'], t['response'])
                           for i, t in trl_info_df.iterrows()]


    trial_types = list(set(set(combined_trial_type)))
    trial_size = trl_data[list(trl_data.keys())[0]].shape[1]
    trials_by_type = {k: [] for k in trial_types}

    for i, t in trl_info_df.iterrows():
        tn = t['trial_number']
        ty = (t['orientation'], t['response'])

        trials_by_type[ty].append(trl_data[tn])

    av_psth = {ty: np.sum(trials_by_type[ty], axis=0) / len(trials_by_type[ty])
               for ty in trial_types}

    Xav = np.hstack([av_psth[t] for t in trial_types])

    print('\nPreprocessing PSTHS for PCA: {}\n'.format(preprocess))
    if preprocess == 'min_max':
        Xav = min_max_scale(Xav, neuron_axis=0)
    elif preprocess == 'z_score':
        Xav_mean = Xav.mean(axis=1)
        Xav_std = Xav.std(axis=1)
        Xav_std[Xav_std == 0] = 1
        Xav = z_score(Xav, neuron_axis=0)

    pca = PCA(n_components=n_components)
    Xp_av = pca.fit_transform(Xav.T).T

    if contrasts == 'paired':
        comparisons = []
        for tt in trial_types:
            ttc = list(tt)
            if ttc[1] == 0:
                ttc[1] = 1
            elif ttc[1] == 1:
                ttc[1] = 0
            comparisons.append((tt, tuple(ttc)))

    elif contrasts == 'all':
        comparisons = []
        for tt in trial_types:
            if tt[1] == 0:
                comparisons = comparisons + [(tt, t) for t in trial_types if t[1] == 1]
            elif tt[1] == 1:
                comparisons = comparisons + [(tt, t) for t in trial_types if t[1] == 0]


    comparisons = [tuple(x) for x in set(map(frozenset, comparisons))]

    # create a dictionary with the first n components of th average pca
    pca_av = {t: None for t in trial_types}
    for kk, type in enumerate(trial_types):
        x = Xp_av[0:n_components, kk * trial_size:(kk + 1) * trial_size]
        pca_av[type] = x

    for type1, type2 in comparisons:
        print(type1, type2)
        a = pca_av[type1]
        b = pca_av[type2]
        d = np.linalg.norm(a - b, axis=0)
        C[mouse_id][area].append(d)


# --- PLOT INDIVIDUAL ANIMALS -------------------------------------------------
err_style = 'ci_band'
f, ax = plt.subplots(1, 1)
for mouse_id, area in datasets:
    #x = C[mouse_id][area]
    #print(x)
    x = np.vstack(C[mouse_id][area])
    sns.tsplot(ax=ax, data=x, time=time,
               color=area_colormap[area], err_style=err_style, ci=95)
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D

custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])



# --- PLOT ACROSS ANIMALS -----------------------------------------------------
err_style = 'ci_band'

Ca = {'V1' : [], 'AL' : []}
for mouse_id, area in datasets:
    for r in C[mouse_id][area]:
        Ca[area].append(r)

# Ca = {'V1' : [], 'AL' : []}
# for mouse_id, area in datasets:
#     Ca[area].append(np.vstack(C[mouse_id][area]).mean(axis=0))


f, ax = plt.subplots(1, 1, figsize=[5, 4])
for area in areas:
    sns.tsplot(ax=ax, data=np.vstack(Ca[area]), time=time,
               color=area_colormap[area], err_style=err_style, ci=95)
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
add_area_legend(ax, areas)
ticks = time[np.mod(time, 25) == 0]
add_time(ax, time)

if save_plots:
    dir = './plots/starecase/final'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'PCA_traj_distance_stim{}_n{}.{}'.format(st, n_components,
                                                 plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    f.savefig(os.path.join(dir, plot_name), dpi=dpi)


