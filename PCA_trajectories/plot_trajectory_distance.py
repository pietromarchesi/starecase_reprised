# -*- coding: utf-8 -*-
import os
import pickle
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
from starecase.starecase_plotting_style import *
from starecase.constants import *

home         = os.path.expanduser('~')
data_dir     = os.path.join(home,'data/STARECASE/preprocessed_longfinal/')
results_dir  = os.path.join(home, 'data/STARECASE/results_starecase/PCA_trajectories/')
plots_dir    = '/home/pietro/data/STARECASE/results_starecase/plots/'


n_components  = 30
stim_types    = [1, 3]
traj_type     = 'long' # 'long', 'single_trials', 'long_alt

save_plots    = True
dpi           = 1000
plot_format   = 'pdf'
time          = np.arange(-12, 76)

if traj_type == 'long_alt' or traj_type == 'psth_alt':
    C_file = 'PCA_trajectory_distance_{}_n{}_s13.pkl'.format(traj_type,
                    n_components)
    C  = pickle.load(open(os.path.join(results_dir, C_file), 'rb'))['results']

else:
    C = {s : None for s in stim_types}
    for stim in stim_types:
        C_file = 'PCA_trajectory_distance_{}_n{}_s{}.pkl'.format(traj_type,
                        n_components, stim)

        C_stim  = pickle.load(open(os.path.join(results_dir, C_file), 'rb'))['results']

        C[stim] = C_stim


# --- PLOT ACROSS ANIMALS -----------------------------------------------------
smooth         = False
sigma          = 2
average_trials = True
Ca = {s :{'V1' : [], 'AL' : []} for s in stim_types}

if average_trials:
    for stim in stim_types:
        for mouse_id, area in datasets:
            r = np.vstack(C[stim][mouse_id][area]).mean(axis=0)
            if smooth:
                r = gaussian_filter1d(r, sigma=sigma)
            Ca[stim][area].append(r)

else:
    for mouse_id, area in datasets:
        for stim in stim_types:
            for r in C[stim][mouse_id][area]:
                if smooth:
                    r = gaussian_filter1d(r, sigma=sigma)
                Ca[stim][area].append(r)


Ca = {s : {area : np.vstack(Ca[s][area]) for area in areas} for s in stim_types}


err_style = 'ci_band'
f, axes = plt.subplots(1, 2, figsize=[8, 4], sharey=True)
for i, stim in enumerate(stim_types):
    for area in areas:
        sns.tsplot(ax=axes[i], data=np.vstack(Ca[stim][area]), time=time,
                   color=area_colormap[area], err_style=err_style, ci=68)
    add_stim_to_plot(axes[i])
    add_time(axes[i], time)
add_area_legend(axes[1], areas)
sns.despine(fig=f, top=True, right=True)
axes[0].set_ylabel('Average hit/miss distance (a.u.)')
axes[0].set_title('V')
axes[1].set_title('AV')
plt.tight_layout()

if save_plots:
    plot_name = 'plot1_PCA_trajectory_distance_single_trials_n{}.{}'.format(
                 n_components, plot_format)
    f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)


# --- DISTANCE BETWEEN V1 and AL HIT/MISS DIFFERENCE in V vs AV TRIALS --------
n_boot = 500
f, ax = plt.subplots(1, 1, figsize=[6, 4])

for stim in stim_types:
    Y = []
    n_t_al = Ca[stim]['AL'].shape[0]
    n_t_v1 = Ca[stim]['V1'].shape[0]
    for boot in range(n_boot):
        ind1 = np.random.choice(np.arange(0, n_t_al), n_t_al, replace=True)
        ind2 = np.random.choice(np.arange(0, n_t_v1), n_t_v1, replace=True)
        y = Ca[stim]['AL'][ind1, :].mean(axis=0) - Ca[stim]['V1'][ind2, :].mean(axis=0)
        Y.append(y)
    sns.tsplot(ax=ax, data=np.vstack(Y), time=time,
               color=stimtype_colormap[stim], err_style=err_style, ci=95)
ax.axhline(0, ls='--', c='k', alpha=0.5)
add_time(ax, time)
add_stim_to_plot(ax)
add_stimtype_legend(ax, stim_types)
sns.despine(fig=f, top=True, right=True)
ax.set_ylabel('V1-AL difference in hit/miss distance (a.u.)')
plt.tight_layout()


if save_plots:
    plot_name = 'plot2dist_PCA_trajectory_distance_single_trials_n{}.{}'.format(
                 n_components, plot_format)
    f.savefig(os.path.join(plots_dir, plot_name), dpi=dpi)


# # --- PAIRED DIFFERENCE -------------------------------------------------------
# Cb = {s : {m_id : [] for m_id in paired_mouse_ids} for s in stim_types}
#
# for stim in stim_types:
#     for mouse_id in paired_mouse_ids:
#         if smooth:
#             m1 = np.vstack(C[stim][mouse_id]['AL']).mean(axis=0)
#             m2 = np.vstack(C[stim][mouse_id]['V1']).mean(axis=0)
#
#             x = m1 - m2
#             x = gaussian_filter1d(x, sigma=sigma)
#             Cb[stim][mouse_id] = x
#
#
# f, ax = plt.subplots(1, 1)
# for stim in stim_types:
#     for m_id in paired_mouse_ids:
#         ax.plot(time, Cb[stim][m_id], color=stimtype_colormap[stim])
# ax.axhline(0, alpha=0.8, c='grey', ls='--')
# add_stim_to_plot(ax)
# sns.despine(fig=f, top=True, right=True)
#
#
# Cb = {s : [] for s in stim_types}
#
# for stim in stim_types:
#     for mouse_id in paired_mouse_ids:
#         if smooth:
#             m1 = np.vstack(C[stim][mouse_id]['AL']).mean(axis=0)
#             m2 = np.vstack(C[stim][mouse_id]['V1']).mean(axis=0)
#
#             x = m1 - m2
#             x = gaussian_filter1d(x, sigma=sigma)
#             Cb[stim].append(x)
#
#
# f, ax = plt.subplots(1, 1)
# for stim in stim_types:
#     sns.tsplot(ax=ax, data=Cb[stim], time=time, err_style='ci_band', ci='sd',
#                color=stimtype_colormap[stim])
# ax.axhline(0, alpha=0.8, c='grey', ls='--')
# add_stim_to_plot(ax)
# sns.despine(fig=f, top=True, right=True)