# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *
from starecase.constants import *
from starecase.starecase_utils import fit_PCA_on_PSTHS


home         = os.path.expanduser('~')
data_dir     = os.path.join(home,'data/STARECASE/preprocessed_longfinal/')
results_dir  = os.path.join(home, 'data/STARECASE/results_starecase/PCA_trajectories/')


align_event   = 'stim_on'
preprocess    = 'z_score'
n_components  = 5
filter_contr  = False
contrast_thr  = 'auto'
average_vars  = ['response', 'orientation']

save_plots    = True
dpi           = 500
plot_format   = '.png'


if not os.path.isdir(results_dir):
    os.makedirs(results_dir)

for stim_types in [[1], [3]]:

    C = {m_id : {ar : [] for ar in areas} for m_id in mouse_ids}

    pars = {'preprocess': preprocess,
            'n_components': n_components,
            'filter_contr': filter_contr,
            'stim_types': stim_types,
            'average_vars': average_vars}

    print('RUNNING DECODING ROUTINE\n')
    for k, v in pars.items():
        print('{!s:>15} : {!s:<25}'.format(k, v))
    print('\n')

    for mouse_id, area in datasets:

        print('\n\nWorking on dataset: {} {}, stim: {}'.format(mouse_id, area, stim_types))
        # load data
        file_name = 'starecase_session_{}_{}_' \
                    'alignedto_{}.pkl'.format(mouse_id, area, align_event)
        data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

        out = fit_PCA_on_PSTHS(data, stim_types=stim_types,
                              average_vars=average_vars, preprocess='z_score',
                              n_bins_contrast=2, n_bins_rt=2, n_components=n_components)

        pca = out['pca']
        Xp_av = out['projected_psth']
        trial_types = out['trial_type_psth']
        trial_type = out['trial_type']
        trials = out['projected_trials']
        trial_size = out['projected_trials'][0].shape[1]
        time = out['time']
        gt_av = out['gt_av']

        print(np.sum(pca.explained_variance_ratio_))
        hit_trials = [trl for trl, ty in zip(trials, trial_type) if ty[0] == 1]
        miss_trials = [trl for trl, ty in zip(trials, trial_type) if ty[0] == 0]

        C[mouse_id][area] = np.zeros([len(trials), trial_size])

        for i, (trial, t_type) in enumerate(zip(trials, trial_type)):

            distance_trial = []
            if t_type[0] == 0:
                for trial_to_compare in hit_trials:
                    d = np.linalg.norm(trial - trial_to_compare, axis=0)
                    distance_trial.append(d)
            elif t_type[0] == 1:
                for trial_to_compare in miss_trials:
                    d = np.linalg.norm(trial - trial_to_compare, axis=0)
                    distance_trial.append(d)
            D = np.vstack(distance_trial).mean(axis=0)
            C[mouse_id][area][i, :] = D



    out = {'results' : C, 'pars' : pars}
    st = ''.join([str(t) for t in stim_types])
    res_name = 'PCA_trajectory_distance_single_trials_n{}_s{}.pkl'.format(
                    n_components, st)
    destination = os.path.join(results_dir, res_name)
    print('Saving results to {}'.format(destination))
    pickle.dump(out, open(destination, 'wb'))






