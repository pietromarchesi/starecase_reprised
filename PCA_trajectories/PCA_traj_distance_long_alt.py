# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *
from starecase.constants import *

home         = os.path.expanduser('~')
data_dir     = os.path.join(home,'data/STARECASE/preprocessed_longfinal/')
results_dir  = os.path.join(home, 'data/STARECASE/results_starecase/PCA_trajectories/')

align_event   = 'stim_on'

preprocess    = 'z_score'
n_components  = 40

filter_contr  = False
contrast_thr  = 'auto'

average_vars  = ['stim_type', 'response', 'orientation']

stim_types    = [1, 3]

C = {s : {m_id: {ar: [] for ar in areas} for m_id in mouse_ids} for s in stim_types}

for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name     = 'starecase_session_{}_{}_' \
                    'alignedto_{}.pkl'.format(mouse_id, area, align_event)

    data = pickle.load(open(os.path.join(data_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    # 1. select stim types
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df.stim_type, stim_types) ),:]


    # 2. filter for contrast
    if filter_contr:
        if contrast_thr == 'auto':
            contrast_thr_val = trl_info_df.visual_cont.median()

        elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
            contrast_thr_val = contrast_thr
        trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont > contrast_thr_val), :]
        print('Filtered contrast at {}'.format(contrast_thr_val))


    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()

    if np.isin('contrast', average_vars):
        contrast_bins = pd.qcut(trl_info_df['visual_cont'], q=2)
        trl_info_df['contrast'] = contrast_bins.cat.codes

    trial_type = [tuple([t[k] for k in average_vars]) for i, t in
                           trl_info_df.iterrows()]
    trial_types = list(set(set(trial_type)))
    trial_size = trl_data[list(trl_data.keys())[0]].shape[1]

    X = np.hstack([trl_data[n] for n in selected_trials])

    if preprocess == 'min_max':
        X = min_max_scale(X, neuron_axis=0)
    elif preprocess == 'z_score':
        X = z_score(X, neuron_axis=0)

    pca_long = PCA(n_components=n_components)
    Xp = pca_long.fit_transform(X.T).T

    for stim in [1, 3]:

        trials = [Xp[:, trial_size * i: trial_size * (i + 1)] for i, t in
                  enumerate(trial_type) if t[0] == stim]

        trial_type_stim = [t for t in trial_type if t[0] == stim]
        hit_trials = [trl for trl, ty in zip(trials, trial_type_stim) if ty[1] == 1]
        miss_trials = [trl for trl, ty in zip(trials, trial_type_stim) if ty[1] == 0]

        C[stim][mouse_id][area] = np.zeros([len(trials), trial_size])
    
        for i, (trial, t_type) in enumerate(zip(trials, trial_type_stim)):
    
            distance_trial = []
            if t_type[1] == 0:
                for trial_to_compare in hit_trials:
                    d = np.linalg.norm(trial - trial_to_compare, axis=0)
                    distance_trial.append(d)
            elif t_type[1] == 1:
                for trial_to_compare in miss_trials:
                    d = np.linalg.norm(trial - trial_to_compare, axis=0)
                    distance_trial.append(d)
            D = np.vstack(distance_trial).mean(axis=0)
            C[stim][mouse_id][area][i, :] = D
    

out = {'results' : C, 'pars' : None}
st = ''.join([str(t) for t in stim_types])
res_name = 'PCA_trajectory_distance_long_alt_n{}_s{}.pkl'.format(
                n_components, st)
destination = os.path.join(results_dir, res_name)
print('Saving results to {}'.format(destination))
pickle.dump(out, open(destination, 'wb'))
