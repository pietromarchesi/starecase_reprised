# -*- coding: utf-8 -*-
import os
import neo
import quantities as pq
import pickle
import elephant
from elephant import kernels
import numpy as np
import tensortools as tt
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from preprocessing_utils import min_max_scale, z_score
from starecase.starecase_plotting_style import *
from starecase.constants import *

save_dir      = '/home/pietro/data/STARECASE/preprocessed/'
mouse_id      = '38'
area          = 'V1'
align_event   = 'stim_on'

preprocess    = 'z_score'
filter_contr  = False
contrast_thr  = 'auto'
stim_types    = [1, 3]

save_plots    = False
plot_format   = 'png'
dpi           = 300
select        = True

file_name     = 'starecase_session_{}_{}_' \
              'alignedto_{}.pkl'.format(mouse_id, area,
                                                     align_event)

data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

trl_info_df = data['trl_info_df']
trl_data = data['trl_data']
time = data['time']

trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]
trl_info_df = trl_info_df.loc[(np.isin(trl_info_df.trial_type, stim_types) ),:]

# 2. filter for contrast
if filter_contr:
    if contrast_thr == 'auto':
        contrast_thr_val = trl_info_df.visual_cont.median() + trl_info_df.visual_cont.std()/2

    elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
        contrast_thr_val = contrast_thr
    trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont < contrast_thr_val), :]
    print('Filtered contrast at {}'.format(contrast_thr_val))


selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()
contrast_bins = pd.qcut(trl_info_df['visual_cont'], q=4)
trl_info_df['contrast_bin'] = contrast_bins.cat.codes

rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
trl_info_df['rt'] = rt_bins.cat.codes

trl_data = {n : trl_data[n] for n in selected_trials}

combined_trial_type = [(t['orientation'], t['trial_type'],
                        t['rt']) for i, t in trl_info_df.iterrows()]
trials = [trl_data[t['trial_number']] for i, t in trl_info_df.iterrows()]

trial_types = list(set(set(combined_trial_type)))
trial_size = trl_data[list(trl_data.keys())[0]].shape[1]
trials_by_type = {k : [] for k in trial_types}

for i, t in trl_info_df.iterrows():

    tn = t['trial_number']
    ty = (t['orientation'], t['trial_type'], t['rt'])

    trials_by_type[ty].append(trl_data[tn])

av_psth = {ty: np.sum(trials_by_type[ty], axis=0) / len(trials_by_type[ty])
           for ty in trial_types}

Xav = np.hstack([av_psth[t] for t in trial_types])

Xav_mean = Xav.mean(axis=1)
Xav_std = Xav.std(axis=1)
Xav_std[Xav_std == 0] = 1
print('\nPreprocessing PSTHS for PCA: {}\n'.format(preprocess))
if preprocess == 'min_max':
    Xav = min_max_scale(Xav, neuron_axis=0)
elif preprocess == 'z_score':
    Xav = z_score(Xav, neuron_axis=0)

pca = PCA(n_components=15)
Xp_av = pca.fit_transform(Xav.T).T


# --- PLOT --------------------------------------------------------------------

def add_stim_to_plot_aligned_to_stim(ax):
    start_stim  = 0
    end_stim    = 27
    shade_alpha = 0.2
    lines_alpha = 0.8
    ax.axvspan(start_stim, end_stim, alpha=shade_alpha,
               color='gray')
    ax.axvline(start_stim, alpha=lines_alpha, color='gray', ls='--')
    ax.axvline(end_stim, alpha=lines_alpha, color='gray', ls='--')


def add_stim_to_plot_aligned_to_lick(ax):
    pass
    # start_stim  = 0
    # end_stim    = 27
    # shade_alpha = 0.2
    # lines_alpha = 0.8
    # ax.axvspan(start_stim, end_stim, alpha=shade_alpha,
    #            color='gray')
    # ax.axvline(start_stim, alpha=lines_alpha, color='gray', ls='--')
    # ax.axvline(end_stim, alpha=lines_alpha, color='gray', ls='--')

rt_colormap = sns.color_palette("Blues", n_colors=4)[1::2]

fig, axes = plt.subplots(2, 2, figsize=[10, 6], sharey='row', sharex='col')

# iterate over the first 9 components
for comp in range(4):
    ax = axes.flatten()[comp]
    for kk, type in enumerate(trial_types):
        # for every trial type, select the part of the component
        # which corresponds to that trial type:
        x = Xp_av[comp, kk * trial_size :(kk+1) * trial_size]
        label = '{}'.format(type)
        #ax.plot(time, x, label=label, c=stimtype_colormap[type[2]])

        ax.plot(time, x, label=label, c=rt_colormap[type[2]])
        if np.isin(comp, [0]):
            ax.plot(time, x, label=label, c=stimtype_colormap[type[1]])
        # elif np.isin(comp, [1, 2, 5]):
        #      ax.plot(time, x, label=label, c=orientation_colormap[type[0]])
        #else:
        #    ax.plot(time, x, label=label, c=contrast_colormap[type[3]])
        # else:
        #     ax.plot(time, x, label=label, c=stimtype_colormap[type[2]])
    add_stim_to_plot(ax)

sns.despine(fig=fig, right=True, top=True)
#axes[0, 0].legend()
axes[1, 0].set_ylabel('PCA components')
plt.tight_layout()


if save_plots:
    dir = './plots/starecase'
    st = ''.join([str(t) for t in stim_types])
    plot_name = 'PCA_components_w_contrast_{}_{}.{}'.format(mouse_id, area, plot_format)
    if not os.path.isdir(dir):
        os.makedirs(dir)
    fig.savefig(os.path.join(dir, plot_name), dpi=dpi)



# --- EXPLAINED VARIANCE ------------------------------------------------------
f, ax = plt.subplots(1, 1, figsize=[3, 3])
ax.plot(pca.explained_variance_ratio_)
ax.set_ylabel('Exaplined variance ratio')
ax.set_xlabel('# components')
ax.set_xticks(range(10))
sns.despine(right=True, top=True)
plt.tight_layout()




# --- DISTANCE BETWEEN AVERAGE TRAJECTORIES -----------------------------------
C = {m_id : {ar : [] for ar in areas} for m_id in mouse_ids}

n_components = 10
comparisons = []
for tt in trial_types:
    if tt[2] == 0:
        comparisons = comparisons + [(tt, t) for t in trial_types if t[2] == 1]
    elif tt[2] == 1:
        comparisons = comparisons + [(tt, t) for t in trial_types if t[2] == 0]

comparisons = [tuple(x) for x in set(map(frozenset, comparisons))]
comparisons.sort()

# create a dictionary with the first n components of the average pca
pca_av = {t : None for t in trial_types}
for kk, type in enumerate(trial_types):
    x = Xp_av[0:n_components, kk * trial_size :(kk+1) * trial_size]
    pca_av[type] = x


for type1, type2 in comparisons:
    print(type1, type2)
    a = pca_av[type1]
    b = pca_av[type2]
    d = np.linalg.norm(a - b, axis=0)
    C[mouse_id][area].append(d)

err_style = 'ci_band'
f, ax = plt.subplots(1, 1)

x = np.vstack(C[mouse_id][area])
sns.tsplot(ax=ax, data=x, time=time,
           color=area_colormap[area], err_style=err_style)
add_stim_to_plot(ax)
sns.despine(fig=f, top=True, right=True)
from matplotlib.lines import Line2D

custom_lines = [Line2D([0], [0], color=area_colormap['V1'], lw=4),
                Line2D([0], [0], color=area_colormap['AL'], lw=4)]
ax.legend(custom_lines, ['V1', 'AL'])


plt.tight_layout()
sns.despine(fig=f, right=True, top=True)
ax.legend()



# --- 3D PLOT OF AVERAGE PROJECTION -------------------------------------------

# component_x = 0
# component_y = 1
# component_z = 2
# zero = 10
# sigma=4
# stim_duration=27
# size_time_dots=30
# from scipy.ndimage.filters import gaussian_filter1d
#
# fig = plt.figure(figsize=[7, 7])
# ax = fig.add_subplot(1, 1, 1, projection='3d')
#
# for kk, t_type in enumerate(trial_types):
#     # for every trial type, select the part of the component
#     # which corresponds to that trial type:
#     x = Xp_av[component_x, kk * trial_size:(kk + 1) * trial_size]
#     y = Xp_av[component_y, kk * trial_size:(kk + 1) * trial_size]
#     z = Xp_av[component_z, kk * trial_size:(kk + 1) * trial_size]
#
#     x = gaussian_filter1d(x, sigma=sigma)
#     y = gaussian_filter1d(y, sigma=sigma)
#     z = gaussian_filter1d(z, sigma=sigma)
#
#     ax.plot(x, y, z, c=orientation_colormap[t_type[0]],
#             ls=response_linestyle[t_type[1]])
#     ax.scatter(x[zero], y[zero], z[zero],
#                c=orientation_colormap[t_type[0]],
#                s=size_time_dots)
#     ax.scatter(x[zero+stim_duration], y[zero+stim_duration], z[zero+stim_duration],
#                edgecolors=orientation_colormap[t_type[0]],facecolors='none',
#                s=size_time_dots+10)
# ax.grid(False)
# #ax.view_init(elev=17, azim=-14)
# ax.view_init(elev=17, azim=20)
# ax.xaxis.set_alpha(0.0)
# fig.tight_layout()
#
#
#
# if save_plots:
#     dir = './plots/starecase'
#     st = ''.join([str(t) for t in stim_types])
#     plot_name = 'PCA_trajectory_{}_{}.{}'.format(mouse_id, area, plot_format)
#     if not os.path.isdir(dir):
#         os.makedirs(dir)
#     fig.savefig(os.path.join(dir, plot_name), dpi=dpi)
#
#
#
#
# # --- PROJECT INDIVIDUAL TRIALS -----------------------------------------------
# if False:
#     gt_av = {comp : {t_type : [] for t_type in trial_types}
#           for comp in range(n_components)}
#
#     for comp in range(n_components):
#         for i, (t_type, trl) in enumerate(zip(combined_trial_type, trials)):
#             centered_trial = (trl.T - Xav_mean) / Xav_std
#
#             t_pca = np.matmul(centered_trial, pca.components_[comp, None].T).T
#
#             t_pca_2 = pca.transform(centered_trial).T[comp, None, :]
#
#             np.testing.assert_array_almost_equal(t_pca, t_pca_2)
#
#             gt_av[comp][t_type].append(t_pca)
#
#
#
#     tsplot_err_style = 'ci_band'
#     tsplot_ci=95
#     plot_events = ['CUE-ON', 'GO-ON', 'SR', 'OT', 'RW-ON']
#
#
#     f, axes = plt.subplots(3, 2, figsize=[13, 6], sharey='row', sharex='col')
#     try:
#         n_subplots = axes.shape[0] * axes.shape[1]
#     except IndexError:
#         n_subplots = axes.shape[0]
#     n_comp_to_plot = min(n_components, n_subplots)
#
#     for comp in range(n_comp_to_plot):
#         for t_type in trial_types:
#             ax = axes.flatten()[comp]
#             x = np.vstack(gt_av[comp][t_type])
#             sns.tsplot(x, time=time,
#                        ax=ax,
#                        err_style=tsplot_err_style,
#                        ci=tsplot_ci,
#                        color=orientation_colormap[t_type[0]],
#                        ls=response_linestyle[t_type[1]])
#
#         add_stim_to_plot(ax)
#
#     sns.despine(fig=f, right=True, top=True)
#     plt.tight_layout()
