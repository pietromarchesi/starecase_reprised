import os
import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from starecase.constants import *
from starecase.starecase_plotting_style import *
from starecase.starecase_utils import fit_PCA_on_PSTHS
from sklearn.metrics import roc_auc_score

save_dir     = '/home/pietro/data/STARECASE/preprocessed_longlong/'
align_event  = 'stim_on'
stim_types   = [3]
orientations = [90, 330, 210]

save_plots   = True
plot_format  = 'png'
dpi          = 500
filter_contr = False
contrast_thr = 'auto'
target       = 'response'

AUCS         = []

for mouse_id, area in datasets:

    print('Working on dataset: {} {}'.format(mouse_id, area))

    file_name = 'starecase_session_{}_{}_' \
                'alignedto_{}.pkl'.format(mouse_id, area,
                                          align_event)

    data = pickle.load(open(os.path.join(save_dir, file_name), 'rb'))

    trl_info_df = data['trl_info_df']
    trl_data = data['trl_data']
    time = data['time']

    if target == 'rt':
        trl_info_df = trl_info_df.loc[trl_info_df['response'] == 1, :]

    trl_info_df = trl_info_df.loc[np.isin(trl_info_df['orientation'], orientations), :]
    trl_info_df = trl_info_df.loc[(np.isin(trl_info_df['stim_type'], stim_types) ),:]

    if filter_contr:
        if contrast_thr == 'auto':
            contrast_thr_val = trl_info_df.visual_cont.median() #+ trl_info_df.visual_cont.std()
        elif isinstance(contrast_thr, int) or isinstance(contrast_thr, float):
            contrast_thr_val = contrast_thr
        trl_info_df = trl_info_df.loc[(trl_info_df.visual_cont < contrast_thr_val),:]
        print('Filtered contrast at {}'.format(contrast_thr_val))

    selected_trials = trl_info_df.loc[:, 'trial_number'].as_matrix()

    rt_bins = pd.qcut(trl_info_df['rel_lick_frame'], q=2)
    trl_info_df['rt'] = rt_bins.cat.codes
    trl_data = {n : trl_data[n] for n in selected_trials}

    trials = [trl_data[t['trial_number']] for i, t in trl_info_df.iterrows()]

    if target == 'rt':
        y_true = trl_info_df['rt'].tolist()
    elif target == 'response':
        y_true = trl_info_df['response'].tolist()

    n_neurons = trials[0].shape[0]
    n_time_points = trials[0].shape[1]

    AUC = np.zeros([n_neurons, n_time_points])

    for tp in range(n_time_points):
        X = np.hstack([tr[:, None, tp] for tr in trials]).T

        for neuron_ind in range(n_neurons):

            y_score = X[:, neuron_ind]

            roc = roc_auc_score(y_true, y_score, average=None)
            AUC[neuron_ind, tp] = roc

    AUCS.append(AUC)


f, ax = plt.subplots(1, 1)

for AUC, (mouse_id, area) in zip(AUCS, datasets):
    sel = np.where(AUC[:, 40:67].mean(axis=1) > 0.50)[0]
    sns.tsplot(AUC[sel, :], time=time, ax=ax, err_style='ci_band',
               color=area_colormap[area])
add_stim_to_plot(ax)


al, v1 = [], []
for AUC, (mouse_id, area) in zip(AUCS, datasets):
    sel = np.where(AUC[:, 40:67].mean(axis=1) > 0.50)[0]
    if area == 'AL':
        al.append(AUC[sel, :])
    elif area == 'V1':
        v1.append(AUC[sel, :])

f, ax = plt.subplots(1, 1)
sns.tsplot(np.vstack(v1), time=time, ax=ax, err_style='ci_band',
           color=area_colormap['V1'])
sns.tsplot(np.vstack(al), time=time, ax=ax, err_style='ci_band',
           color=area_colormap['AL'])
add_stim_to_plot(ax)